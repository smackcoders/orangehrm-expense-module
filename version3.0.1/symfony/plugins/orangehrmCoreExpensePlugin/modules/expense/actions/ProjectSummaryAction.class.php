<?php

class ProjectSummaryAction extends orangehrmAction 
{
	    protected $ProjectSummaryService;

	    public function execute($request) 
	    {
		// 	Set Session for pagination, if not set

		if($_SESSION['smackExpenseSummary'] == '' || !isset($_SESSION['smackExpenseSummary']) || empty($_SESSION['smackExpenseSummary']) || $_REQUEST['smack'] == 'First')
		{
			$_SESSION['smackProjectSummary'] = 0;
		}

		$this->values = "";
//print('<pre>');print_r($_SESSION);
		if($_SESSION['isAdmin'] == 'Yes') 
		{
				$this->values = ExpenseDao::getAdminProjectSummary($_REQUEST['page']);
		}
		else 
		{
				$this->values = ExpenseDao::getProjectSummary($_REQUEST['page'], $_SESSION['empNumber']);
		}
	}
}

