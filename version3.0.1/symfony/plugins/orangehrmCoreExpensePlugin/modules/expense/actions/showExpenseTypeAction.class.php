<?php

class showExpenseTypeAction extends orangehrmAction {

	    protected $ExpenseService;

	    public function execute($request) 
	    {
		if($_SESSION['isAdmin'] == 'Yes') 
		{
			$this->values = ExpenseTypeDao::getExpenseType();
			if($_POST['selectAll'] != '' || $_POST['selectExpense'] != '')
			{ 
				$this->check = ExpenseTypeDao::deleteExpense($_POST['selectExpense']);
				if($this->check == true)
				{
					$_SESSION['messsage'] = "delete";
					$this->redirect("expense/showExpenseType");
				}
			}
		}
		else
		{
			die('You Dont have permission to view this page');
		}
	    }

}

