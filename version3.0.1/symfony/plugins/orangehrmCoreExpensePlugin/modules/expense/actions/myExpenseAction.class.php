<?php

class myExpenseAction extends orangehrmAction {

	    protected $MyExpenseService;

	    public function execute($request) 
	    {
		$this->values = "";
		$_SESSION['expense_search'] = '';
		$this->expenseType = ExpenseDao::getAllExpenseType();

		// 	Set Session for pagination, if not set

		if( $_SESSION['smackExpensePage'] == '' || !isset($_SESSION['smackExpensePage']) || empty($_SESSION['smackExpensePage']) || $_REQUEST['smack'] = 'First')
		{
			$_SESSION['smackExpensePage'] = 0;
		}

		if($_POST['action'] == 'save')
		{
			$empId = $_POST['empId'];
			unset($_POST['action']);
			unset($_POST['empId']);
			ExpenseDao::updateExpense($_POST, $empId);
			$_SESSION['messsage'] = 'update';
		}

		$this->status_expense = array('0' => 'Cancelled','1' => 'Pending', '-1' => 'Rejected', '2' => 'Accepted');
		$this->actions = array('none' => 'Select Action', '0' => 'Cancel');

		if($_REQUEST['search'] == true)
		{
			unset($_REQUESt['btnSearch']);
			unset($_REQUESt['search']);
			$this->myExpense = ExpenseDao::SearchMyExpense($_REQUEST, ExpenseDao::getEmployeeIdById($_SESSION['user']),$_REQUEST['page']);	
		}
		else
		{
			$this->myExpense = ExpenseDao::MyExpense(ExpenseDao::getEmployeeIdById($_SESSION['user']),$_REQUEST['page']);	
		}
            }
}

