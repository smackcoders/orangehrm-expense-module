<?php

class ExpenseSummaryAction extends orangehrmAction 
{
	    protected $ExpenseSummaryService;

	    public function execute($request) 
	    {
		// 	Set Session for pagination, if not set

		if($_SESSION['smackExpenseSummary'] == '' || !isset($_SESSION['smackExpenseSummary']) || empty($_SESSION['smackExpenseSummary']) || $_REQUEST['smack'] == 'First')
		{
			$_SESSION['smackExpenseSummary'] = 0;
		}

		$this->values = "";

		if($_SESSION['isAdmin'] == 'Yes') 
		{
			$this->values = ExpenseDao::getAdminExpenseSummary($_REQUEST['page']);
		}
		else
		{
			$this->values = ExpenseDao::getExpenseSummary($_REQUEST['page'], $_SESSION['empNumber']);
		}
	}
}

