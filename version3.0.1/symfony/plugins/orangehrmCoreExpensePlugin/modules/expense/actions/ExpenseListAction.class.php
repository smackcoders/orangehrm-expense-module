<?php

class ExpenseListAction extends orangehrmAction 
{
	    protected $ExpenseListService;

	    public function execute($request) 
	    {
		// 	Set Session for pagination, if not set

		if( $_SESSION['smackExpensePage'] == '' || !isset($_SESSION['smackExpensePage']) || empty($_SESSION['smackExpensePage']) || $_REQUEST['smack'] == 'First')
		{
			$_SESSION['smackExpensePage'] = 0;
			$_SESSION['smack_finalorder'] = "order by created_time ASC";
		}

		$this->values = "";
		$this->filterExpensevalue = "";
		$this->filtervalue = "";
		$_SESSION['expense_search'] = '';

		$this->status_expense = array('0' => 'Cancelled','1' => 'Pending',  '-1' => 'Rejected', '2' => 'Accepted');		
		$this->actions        = array('none' => 'Select Action', '2' => 'Approve', '-1' => 'Reject', '0' => 'Cancel');
		$this->afterselection = array('none' => 'Select Action', '0' => 'Cancel');
		$this->emp_tosearch   = ExpenseDao::makeEmployeesarray();

		$this->expenseType = ExpenseTypeDao::getExpenseType();
		if($_REQUEST['search'] == true)
		{
			unset($_REQUEST['btnSearch']);
			unset($_REQUEST['search']);
			unset($_REQUEST['smack']);
			$this->Search_Expense = true;
			if($_SESSION['isAdmin'] == 'Yes')
			{
				$this->values = ExpenseDao::searchAdminExpenseList($_REQUEST,$_REQUEST['page']);
			}
			else
			{
				$this->values = ExpenseDao::searchExpenseList($_REQUEST,$_REQUEST['page']);
			}
		}
		else if($_REQUEST['action'] == 'save')
		{
			unset($_REQUEST['action']);
			unset($_REQUEST['empId']);
			unset($_REQUEST['smack']);
			$remaining_amt = ExpenseDao::checkExpenseBalance($_REQUEST);
			ExpenseDao::updateExpenseList($remaining_amt);
			$_SESSION['smack_message'] = "update";
			$this->redirect('expense/ExpenseList');
		}
		else if(isset($_REQUEST['page']))
		{
			if($_SESSION['isAdmin'] == 'Yes')
			{
				$this->values = ExpenseDao::getAdminExpense($_REQUEST['page']);
			}
			else
			{
				$this->values = ExpenseDao::getExpense($_REQUEST['page']);
			}
		}
		else
		{
			if($_SESSION['isAdmin'] == 'Yes')
			{
				$this->values = ExpenseDao::getAdminExpense();
			}
			else
			{
				$this->values = ExpenseDao::getExpense();
			}
		}
	}
}

