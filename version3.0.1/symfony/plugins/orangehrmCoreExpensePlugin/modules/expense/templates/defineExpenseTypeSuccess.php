<script type = "text/javascript">
function checkExpenseNam()
{
	var exname = document.getElementById('name').value;
	var project = document.getElementById('project').value;
	var x = document.getElementById("project").selectedIndex;
	var projectname = document.getElementById('project').options;
	var data;
	if(exname.length != 0 && project != 'none')
	{
		data = "&name="+exname+"&projectid="+project;
		jQuery.ajax({
			url: 'checkExpenseName',
			type: 'post',
			data: data,
			success: function(response)
			{
				if(response == 2) 
				{
					var msg = document.getElementById("showMessage");
        		                msg.style.display = "block";
	                	        msg.className = "message error";
					msg.innerHTML = exname+" Already Exist for the Project "+projectname[x].text+" <a class='messageCloseButton' href='#'>Close</a>";
					document.getElementById('name').value = "";
					return false;
				}
				else
				{
					return true;
				}
			}
		});
	}
}
</script>
  
<html>
<body>
<div style = "padding-left:10px;padding-top:10px;">
<div class = "formpage">
<div id="search-results" class="box">
<form name = "frmProjectType" action = "" method = "post" id = "frmProjectType" onsubmit = "return checkCreateProject();" >
       <input type = "hidden" name = "action" value = "create">
       <div class = "head">
		<h1> Create Expense Project </h1>
	</div>

	<div class = "inner">
	<div id = "showMessage"> </div>

	<fieldset>
		<ol>
			<li>
				<label> Expense Name <em>*</em> </label>	<input type = "text" name = "name" id = "name" onblur = "checkExpenseNam();">
			</li>
			<li>
                                <label> Project Name <em>*</em> </label>        <select id = "project" name = "project" onchange = "checkExpenseNam();"> <option value = "none" selected> Select </option>  <?php echo htmlspecialchars_decode($Project); ?> </select>
                        </li>
			<li>
                                <label> Description </label>         <textarea name = "desc" id = "desc"> </textarea>
                        </li>
			<li>
                                <label> Budget <em>*</em> </label>   <input type = "text" name = "budget" id = "budget">
			</li>
			<li>
				 <label> Update Monthly </label> <input type = "checkbox" name = "automation" id = "automation" checked = "checked" > 
                        </li>
		</ol>
	</fieldset>
	<p>  <input type = "submit"  value = "Save"  class="applybutton" id = "saveButton" > <input id="backButton" class="cancel" type="button" value="Cancel" name="backButton" onclick = "cancelExpenseAction();"> </p>
	</div>
</div>
</div>
</div>
</form>
</body>
</html>
<script type = "text/javascript">
function cancelExpenseAction()
{
	window.location.href = "showExpenseType";
}

function checkCreateProject()
{
	var exname = document.getElementById('name').value;
	var budget = document.getElementById('budget').value;
	var x = document.getElementById("project").selectedIndex;
	var project = document.getElementById('project').value;
	var projectname = document.getElementById('project').options;
	var checkBud = isNaN(budget.trim());	
	if(exname.trim().length == 0 || budget.trim().length == 0 || project == 'none') 
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Please fill Mandatory Fields <a class='messageCloseButton' href='#'>Close</a>";
		return false;
	}
	else if(checkBud == true)
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Budget should be in Number <a class='messageCloseButton' href='#'>Close</a>";
		return false;
	}
	else if(budget.trim().length > 10)
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Number should be Less than 10 <a class='messageCloseButton' href='#'>Close</a>";
		return false;
	}
	else
	{
		var data;
		if(exname.length != 0 && project != 'none')
		{
			data = "&name="+exname+"&projectid="+project;
			jQuery.ajax({
				url: 'checkExpenseName',
				type: 'post',
				data: data,
				success: function(response)
				{
					if(response == 2) 
					{
						var msg = document.getElementById("showMessage");
		                                msg.style.display = "block";
		                                msg.className = "message error";
						msg.innerHTML = exname+" Already Exist for the project "+projectname[x].text+" <a class='messageCloseButton' href='#'>Close</a>";
						return false;
					}
					else
					{
						return true;
					}
				}
			});
		}
	}
}
</script>
