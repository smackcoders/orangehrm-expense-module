<?php echo javascript_include_tag('orangehrm.datepicker.js'); ?>

<style>
.moveRight      {
        margin: 0 30px 0 0;
}

.moveLabel      {
        margin: 0 10px 0 0;
}
</style>

<script type = "text/javascript">
function clearSearch()
{
	document.getElementById('fromdate').value = "";
	document.getElementById('todate').value = "";
	document.getElementById('pending').checked = false;
	document.getElementById('approved').checked = false;
	document.getElementById('cancelled').checked = false;
	document.getElementById('rejected').checked = false;
}

function checkSearch()
{
	var from = document.getElementById('fromdate').value;
	var to = document.getElementById('todate').value;
	var extype = document.getElementById('extype').value;
	var pending = document.getElementById('pending').checked;
	var approved = document.getElementById('approved').checked;
	var cancelled = document.getElementById('cancelled').checked;
	var rejected = document.getElementById('rejected').checked;
	if(from == 'yy-mm-dd' && to == 'yy-mm-dd' && pending == false && approved == false && cancelled == false && rejected == false && extype == 'none')
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Please Select any values to search";
		return false;
	}
	else if(from != 'yy-mm-dd' && to != 'yy-mm-dd')
	{
		if(from > to)
		{
			var msg = document.getElementById("showMessage");
        	        msg.style.display = "block";
	                msg.className = "message error";
			msg.innerHTML = "From date should be greater than To date";
			return false;			
		}
	}
	else
	{
		return true;
	}
}
</script>

<script type = "text/javascript">

var datepickerDateFormat = 'yy-mm-dd';
$(document).ready(function(){
var rDate = trim($("#todate").val());
if (rDate == '') {
$("#todate").val(datepickerDateFormat);
}
//Bind date picker
daymarker.bindElement("#todate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#todate_Button').click(function(){
daymarker.show("#todate");
});
});

$(document).ready(function(){
var rDate = trim($("#fromdate").val());
if (rDate == '') {
$("#fromdate").val(datepickerDateFormat);
}
//Bind date picker
daymarker.bindElement("#fromdate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#fromdate_Button').click(function(){
daymarker.show("#fromdate");
});
});

</script>  
<?php
	// Making redirection for search

	$redirect_next = ExpenseDao::searchnextRedirect();
	$redirect_prev = ExpenseDao::searchprevRedirect();
	if($expenseType)
	{
		$expenseTypes = '<select name = "extype" id = "extype" > <option value = "none"> Select </option>';
		foreach($expenseType as $singleExpenseType)
		{ 
			if($singleExpenseType['expensetypeId'] == $_SESSION['expense_search']['expense_type_id'])	
			{
				$expenseTypes = $expenseTypes."<option selected value = \"".$singleExpenseType['expensetypeId']."\">".$singleExpenseType['expenseName']."</option>";
			}
			else
			{
				$expenseTypes = $expenseTypes."<option value = \"".$singleExpenseType['expensetypeId']."\">".$singleExpenseType['expenseName']."</option>";
			}
		}
		$expenseTypes = $expenseTypes."</select>";
	}

?>

<html>
<body>
<div class = "box searchForm">
	<div class="formWrapper">
		<form name = "searchExpenseList" onsubmit = "return checkSearch();" action = "#" method = "post" id = "frmExpense">
			<div class="head">
				<h1> Expense List </h1>
			</div>
			<div class = "inner">
				<div id = "showMessage"> </div>
<?php if($_SESSION['messsage'] != '') { ?>
	<div  id="messagebar" class="messageBalloon_success"> 
		<?php if($_SESSION['messsage'] == 'create') { ?> <div class="message success" > <h2> Applied Expense Successfully <a class="messageCloseButton" href="#">Close</a> </h2> </div> <?php  }
			else if($_SESSION['messsage'] == 'update') { ?> <div class="message success"> <h2> Expense Updated Successfully </h2> <a class="messageCloseButton" href="#">Close</a> </div> <?php  } 
		      $_SESSION['messsage'] = ''; ?> </div>  <?php } ?>

			<table> 
				<input type = "hidden" name = "search" value = "true">
				<tr height = "35px;">
					<td width = "30%"> <label> Expense Name </label> </td> 
					<td> <?php echo htmlspecialchars_decode($expenseTypes); ?>
				</tr>
				<tr height = "35px;">
					<td width = "25%"> <label> From Date </label> </td>
					<td> <input type = "textbox" readonly name = "fromdate" <?php if($_SESSION['expense_search']['fromdate']) { ?> value="<?php echo $_SESSION['expense_search']['fromdate'] ?>" <?php  } ?> id = "fromdate"> </td> 
					<td width = "30%"> </td>
				</tr>
				<tr height = "35px;">
					<td width = "25%"> <label> To Date </label> </td> 
					<td>
						 <input type = "textbox" readonly name = "todate" id = "todate" <?php if($_SESSION['expense_search']['todate']) { ?> value="<?php echo $_SESSION['expense_search']['todate'] ?>" <?php  } ?> > 
					</td> 
				</tr>
				<tr height = "35px;">
				        <td width = "25%"> Expense With status </td>
					<td width = "40%"> <span class = "moveLabel">  Pending </span> <span class = "moveRight"> <input type = "checkbox" name = "pending" id = "pending" <?php if($_SESSION['expense_search']['pending']) { ?> checked <?php  } ?> > </span> 
					<span class = "moveLabel"> Approved </span>  <span class = "moveRight"> <input type = "checkbox" name = "approved" id = "approved" <?php if($_SESSION['expense_search']['approved']) { ?> checked <?php  } ?>> </span>
					<span class = "moveLabel"> Cancelled </span>  <span class = "moveRight"> <input type = "checkbox" name = "cancelled" <?php if($_SESSION['expense_search']['cancelled']) { ?> checked <?php  } ?> id = "cancelled"> </span>
					 <span class = "moveLabel"> Rejected </span> <span class = "moveRight"> <input type = "checkbox" name = "rejected" id = "rejected" <?php if($_SESSION['expense_search']['rejected']) { ?> checked <?php  } ?>> </span> </td>
				</tr>
			</table>
			<div class="buttonWrapper">
				<input id="btnSearch" class="searchbutton" type = "submit" value="Search" name="btnSearch" >
				<input id="btnReset" class="clearbutton" type="button" onclick = "clearSearch();" value="Reset" name="btnReset">
				<input id="pageNo" type="hidden" value="" name="pageNo">
			</div>
		</form>
		</div>
	</div>
</div>

<div id="search-results" class="box noHeader">
<div class = "inner">
<div id = "tableWrapper">
<form name = "frmExpense" action = "" method = "post" id = "frmExpense" style = "width:auto;" >
       <input type = "hidden" name = "action" value = "save">
       <input type = "hidden" name = "empId" value = "<?php echo ExpenseDao::getEmployeeIdById($_SESSION['user']); ?>" >
       <table id="resultTable" class="table hover"> 
		<tr height = "35px;"> 	
			<th> Expense Name </th> 
			<th> Expense Date </th>
			<th> Expense Amount </th>
			<th> Status </th>
			<th> Comment </th>
			<th> Action </th>
		</tr>
<?php $chkeven = 1; ?>
<?php foreach($myExpense as $single) 
      {
		$status = ''; 
		foreach($status_expense as $key => $assign_status)
		{
			if($key == $single['expense_status'])
			{
				$status = $status."<option selected value = '{$key}'> {$assign_status} </option>";
			}
			else
			{
				$status = $status."<option value = '{$key}'> {$assign_status} </option>";
			}
		}
		$status = "<select name = 'status' > <option value = '0'> Select Action </option>".$status."</select>";
		$action = '';
		foreach($actions as $key => $split_action)
		{
			$action = $action."<option value = '{$key}'> {$split_action} </option>";
		}
			$action = "<select name = '{$single['expense_id']}-status' >".$action."</select>";
?>
		<tr <?php if($chkeven % 2 == 0) { ?> class="even" <?php } if($chkeven % 2 == 1) { ?> class="odd" <?php } ?> > 	<?php $chkeven++; ?>
			<td class = "tab_td"> <?php echo ExpenseDao::getExpenseNameById($single['expense_type_id']); ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_date']; ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_amount']; ?> </td>
			<td class = "tab_td"> <?php if($single['expense_status'] == 1) 
				   { 
					echo "Pending"; 
				   }
				   else if($single['expense_status'] == -1) 
				   {
					echo "Rejected"; 
				   } 
				   else if($single['expense_status'] == 0) 
				   { 
					echo "Cancelled"; 
				   } 
				   else if($single['expense_status'] == 2) 
				   {
					echo "Accepted"; 
				   } ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_comments']; ?> </td>
			<td class = "tab_td"> <?php echo $action; ?> </td>
		</tr>
<?php } ?>
 
	</table>
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Save"  class="applybutton" id="saveBtn" > 
		<span style = "margin-left:40%;">  
			<?php if($Search_Expense) {   ?>
				<input id="pageno" class="clearbutton" type="button" onclick = "page_search(this.value);" <?php if($_SESSION['smackExpensePage'] == 0) { ?> disabled = "disabled" <?php } ?> value="Previous" name="btnReset">
				<input id="pageno" class="clearbutton" type="button" onclick = "page_search(this.value);" <?php if($next_disable) { ?> disabled = "disabled" <?php } ?>  value="Next" name="btnReset">  
			<?php	} else {  ?>
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($_SESSION['smackExpensePage'] == 0) { ?> disabled = "disabled" <?php } ?> value="Previous" name="btnReset">
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($next_disable) { ?> disabled = "disabled" <?php } ?>  value="Next" name="btnReset">  <?php } ?>
		</span>
	</div>
</form>
</div>
</div>
		<script type = "text/javascript">
			function page(val)
			{
				if(val == 'Next')
				{
					window.location.href = 'myExpense?page=Next';
				}
				else if(val == 'Previous')
				{
					window.location.href = 'myExpense?page=Previous';
				}
			}

			function page_search(val)
			{
				if(val == 'Next')
				{
					window.location.href = "<?php echo $redirect_next; ?>";
				}
				else if(val == 'Previous')
				{
					window.location.href = "<?php echo $redirect_prev; ?>";
				}
			}
		</script>

