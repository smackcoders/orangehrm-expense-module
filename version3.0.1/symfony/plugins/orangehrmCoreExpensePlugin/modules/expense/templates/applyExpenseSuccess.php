<?php echo javascript_include_tag('orangehrm.datepicker.js'); ?>
<script type = "text/javascript">

function checkapply()
{
	var type = document.getElementById('expense').value;
	var amount = document.getElementById('amount').value;
	var date = document.getElementById('expensedate').value;
	var checkamt = isNaN(amount);
	if(type == 0 || amount.length == 0 || date == 'yy-mm-dd' || date.length == 0)
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Please Fill mandotary fields";
		return false;
	}
	else if(checkamt == true)
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
                msg.innerHTML = "Amount Should be in Number";
                return false;
	}
}
</script>

<script type = "text/javascript">

var datepickerDateFormat = 'yy-mm-dd';
$(document).ready(function(){
var rDate = trim($("#expensedate").val());
if (rDate == '') {
$("#expensedate").val(datepickerDateFormat);
}

//Bind date picker
daymarker.bindElement("#expensedate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#expensedate_Button').click(function(){
daymarker.show("#expensedate");
});
});

</script>

<div class = "formpage">
<div id="leave-list-search" class="box toggableForm">
<form name = "frmExpense" onsubmit = "return checkapply();" action = "" method = "post" id = "frmExpense" >
       <input type = "hidden" name = "action" value = "create"> 
       <input type = "hidden" name = "empId" value = "<?php echo ExpenseDao::getEmployeeIdById($_SESSION['user']); ?>" >
       <div class = "head">
		<h1> Apply Expense </h1>
       </div>
       <div class = "inner"> 
<div id = "showMessage"> </div>

	<?php if($_SESSION['messsage'] != '') { ?>
	<div  id="messagebar"> 
		<?php if($_SESSION['messsage'] == 'create') { ?> <div class="message success" > Applied Expense Successfully <a class='messageCloseButton' href='#'>Close</a> </div> <?php  } 
		      $_SESSION['messsage'] = ''; ?> </div>  <?php } ?>

		<fieldset>
		<ol>
		<li>
			<label> Expense Name <em> * </em> </label>
			 <select name = "expense" id = "expense"> <option value = '0'> Select </option> <?php echo htmlspecialchars_decode($expenseTypes); ?> </select>
		</li>
		<li>
			<label> Date <em> * </em> </label> 
			<input type = "textbox" readonly name = "expensedate" id = "expensedate">
	        </li>
		<li>
		 	<label> Amount <em> * </em> </label> 
			<input type = "text" name = "amount" id = "amount">
		</li>
		<li>
		 	<label> Comments </label>
			<textarea name = "comments" id = "comments"> </textarea> 
		</li>
	</fieldset>
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Save"  class="applybutton" id="saveBtn" > </div>
	</div>
</form>
</div>
</div>

