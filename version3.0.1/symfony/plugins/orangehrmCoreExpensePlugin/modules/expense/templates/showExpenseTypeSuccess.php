<?php echo javascript_include_tag('orangehrm.datepicker.js'); ?>
<style>
.moveRight      {
        margin: 0 30px 0 0;
}

.moveLabel      {
        margin: 0 10px 0 0;
}
</style>

<script type = "text/javascript">
checked=false;
function checkedAll (frm1) 
{
	var aa= document.getElementById('frmProjectType');
	if (checked == false)
        {
        	checked = true
        }
        else
        {
		checked = false
        }
	for(var i =0; i < aa.elements.length; i++) 
	{
		aa.elements[i].checked = checked;
	}
}

function confirmDelete()
{
	var checked = false;
	var check = document.getElementById('frmProjectType');
	for(var i = 0; i < check.elements.length; i ++) 
	{
		checked = check.elements[i].checked;
		if(checked == true)
		{
			break;
		}
	}
	
	if(checked == false)
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		document.getElementById('showMessage').innerHTML = 'Please Select any values to delete <a class="messageCloseButton" href="#">Close</a>';
		return false;
	}

	if(checked == true)
	{
		var r = confirm("Are you sure you want to Delete ?");
		if(r == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	} 
}

function redirect()
{
	window.location.href = "defineExpenseType";
}
</script>
  
<div style = "padding-left:10px;padding-top:10px;">
<div class = "formpage">
<div class = "box searchForm">
<form name = "frmProjectType" id = "frmProjectType" action = "" method = "post" id = "frmProjectType" onsubmit = "return confirmDelete();" >
       <input type = "hidden" name = "action" value = "edit">
       <input type = "hidden" name = "module_name" value = "Expense"> 
       <input type = "hidden" name = "employee_id" value = "<?php echo $userId; ?>"> 
        <div class = "head">
	       <h1> Show Expense Project </h1>
        </div>	
	<div class = "inner"> 	
	<div id = "showMessage"> </div>

	<?php if($_SESSION['messsage'] != '') { ?>
	<div id="messagebar"> 
		<?php if($_SESSION['messsage'] == 'delete') { ?> <div class="message success" >  Expense Type Successfully deleted <a class="messageCloseButton" href="#">Close</a>  </div> <?php  } 
		      else if($_SESSION['messsage'] == 'create') { ?> <div class="message success" > Expense Project Created Successfully <a class="messageCloseButton" href="#">Close</a> </div> <?php  } 
		      else if($_SESSION['messsage'] == 'update') { ?> <div class="message success" >  Expense Project Updated Successfully <a class="messageCloseButton" href="#">Close</a> </div> <?php  } $_SESSION['messsage'] = ''; ?> </div>  <?php } ?>

	<div class = "top">
			<input id="btnAdd" class=""  type="button" value="Add" name="btnAdd" onclick = "redirect();">
			<input id="btnDelete" class="delete"  type="submit" value="Delete" name="btnDel">
			<input id="hdnEditId" type="hidden" value="" name="hdnEditId">
	</div>
	<div id = "tableWrapper">
		<table id="resultTable" class="table hover">
			<tr> 
				<th style = "width:1%" class = "checkbox-col" rowspan="1"> <input type = "checkbox" name = "selectAll" id = "selectAll"  onclick = "checkedAll('frmProjectType')"> </th> 
				<th style="width:99%" rowspan="1"> <span class="headerCell"> Expense Name </span> </th> 
			</tr>

<?php 
foreach($values as $val) 
{
		if($i % 2 == 0)
		{ 
?>			<tr valign="top"> 
				<td> <input type = "checkbox" name = "selectExpense[]" id = "selectExpense" value = "<?php echo $val['expensetypeId'];?>">  </td> <td> <a href = "./editExpenseType/<?php echo $val['expensetypeId']; ?>" > <?php echo $val['expenseName']; ?> </td>
			</tr>
<?php 		} 
		else
		{
?>			<tr  style = "background-color:#EEEEEE" valign="top"> 
				<td style = "width:10%;"> <input type = "checkbox"  name = "selectExpense[]" id = "selectExpense" value = "<?php echo $val['expensetypeId'];?>" >  </td> <td> <a href = "./editExpenseType/<?php echo $val['expensetypeId']; ?>" > <?php echo $val['expenseName']; ?> </td>
			</tr>
<?php		} 
		$i ++;
}
?>
	 </tr>
</table>
</div>
</div>
</div>
</div>
</form>
