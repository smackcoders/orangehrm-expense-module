<style type="text/css">
.error_list {
    color: #ff0000;
}

td
{
	padding-left:5px;
	padding-top:5px;
}

.maincontent
{
    -moz-border-bottom-colors: none;
    -moz-border-image: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: white;
    border-color: #FAD163;
    border-style: solid;
    border-width: 0 2px;
    height: auto;
	width:500px;
}

.outerMost {
    margin-left: 15px;
    margin-top: 15px;
    width: 400px;
}
</style>
<?php echo javascript_include_tag('orangehrm.datepicker.js')?>
<form name = "frmProjectType" action = "" method = "post" id = "frmProjectType" onsubmit = "return checkCreateProject();" >
<input type = "hidden" name = "action" value = "update">
<input type = "hidden" name = "module_name" value = "Expense">
<div id="search-results" class="box"> 
       <input type = "hidden" name = "employee_id" value = "<?php echo $userId; ?>"> 
       <input type = "hidden" name = "expenseid" value = "<?php echo $values['expensetypeId']; ?>"> 
       <div class = "head">
	       <h1> <?php if($_REQUEST['edit'] == 1) { ?> Edit Expense Project <?php } else { echo "Show Expense Project"; } ?> </h1>
	</div>
	<div class = "inner">
	<div id = "showMessage"> </div>
	<fieldset>
                <ol>
                        <li>
				<label> Expense Name <?php if($_REQUEST['edit'] == 1) { ?> <em>*</em> <?php } ?> </label>  <?php if($_REQUEST['edit'] == 1) { ?> <input type = "text" name = "name" id = "name" value = "<?php echo $values['expenseName'] ?>"> <?php } else { echo $values['expenseName']; } ?>
			</li>
			<li>
				<label> Project Name <?php if($_REQUEST['edit'] == 1) { ?> <em>*</em> <?php } ?> </label> <?php if($_REQUEST['edit'] == 1) { ?> <select id = "project" name = "project"> <option value = "none" > Select </option>  <?php echo htmlspecialchars_decode($Project); ?> </select> <?php } else { echo ExpenseDao::getProjectNameById($values['projectId']); } ?> 
			</li>
			<li> 
				<label> Description </label>  <?php if($_REQUEST['edit'] == 1) { ?> <textarea name = "desc" id = "desc">  <?php echo $values['description'] ?> </textarea>  <?php } else { echo $values['description']; } ?> 
			</li>
			<li>
			 	<label> Budget <?php if($_REQUEST['edit'] == 1) { ?> <em>*</em> <?php } ?> </label> <?php if($_REQUEST['edit'] == 1) { ?> <input type = "text" name = "budget" id = "budget" <?php if($values['budget']) { ?> value = "<?php echo $values['budget'] ?>"  <?php } ?> >  <?php } else { echo $values['budget']; } ?> 
			</li> 
			<?php if($values['automation'] == 1) { $auto = 'Yes';  } else { $auto = 'No'; } ?>
			<li> <?php if($_REQUEST['edit'] == 1) { ?> <label> Update Monthly </label> <input type = "checkbox" name = "automation" id = "automation" <?php if($values['automation'] == 1) { ?> checked = "checked" >  <?php }  } else { ?> <label> Update monthly </label> <?php echo $auto;  } ?> </li> 
		</ol>
	</fieldset>
	<?php if($_REQUEST['edit'] == 1) { ?> <div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Update"  class="applybutton" id="saveBtn" > </div> <?php } else { ?> <div class="formbuttons paddingLeft" >  <input type = "button"  value = "Edit"  class="applybutton" id="saveBtn" onclick = "redirect_cur();" > </div> <?php } ?>
</div>
</div>
</div>
</form>
<script type = "text/javascript">
function redirect_cur()
{
	window.location.href = 'editExpenseType?edit=1/<?php echo $id; ?>';
}

function checkCreateProject()
{
	var exname = document.getElementById('name').value;
	var budget = document.getElementById('budget').value;
	//var managers = document.getElementById('managers').value;
	//var users = document.getElementById('users').value;
	var checkBud = isNaN(budget.trim());	
	if(exname.trim().length == 0 || budget.trim().length == 0) 
	{
                var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Please fill Mandatory Fields <a class='messageCloseButton' href='#'>Close</a>";
		return false;
	}
	else if(checkBud == true)
	{
                var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Budget should be in Number <a class='messageCloseButton' href='#'>Close</a>";
		return false;
	}
	else if(budget.trim().length > 10)
	{
                var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Number should be Less than 10 <a class='messageCloseButton' href='#'>Close</a>";
		return false;
	}
	return true;
}
</script>
