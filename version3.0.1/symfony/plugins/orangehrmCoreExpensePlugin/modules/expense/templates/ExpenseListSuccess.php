<style>
.moveRight	{
	margin: 0 30px 0 0;
}

.moveLabel	{
	margin: 0 10px 0 0;
}
</style>
<script type="text/javascript">
function clearSearch()
{
	document.getElementById('expenseList_empName').value = "";
	document.getElementById('expenseList_empId').value = "";
	document.getElementById('fromdate').value = "yy-mm-dd";
	document.getElementById('todate').value = "yy-mm-dd";
	document.getElementById('extype').value = "none";
	document.getElementById('pending').checked = false;
	document.getElementById('approved').checked = false;
	document.getElementById('cancelled').checked = false;
	document.getElementById('rejected').checked = false;
}

function checkSearch()
{
	var exname = document.getElementById('expenseList_empName').value;
	var empId = document.getElementById('expenseList_empId').value.length;
	var type = document.getElementById('extype').value;
	var from = document.getElementById('fromdate').value;
	var to = document.getElementById('todate').value;
	var pending = document.getElementById('pending').checked;
	var approved = document.getElementById('approved').checked;
	var cancelled = document.getElementById('cancelled').checked;
	var rejected = document.getElementById('rejected').checked;
	if((exname == 'Type for hints...' || exname.length == 0) && empId == 0 && type == 'none' && from == 'yy-mm-dd' && to == 'yy-mm-dd' && pending == false && approved == false && cancelled == false && rejected == false )
	{
		var msg = document.getElementById("showMessage");
                msg.style.display = "block";
                msg.className = "message error";
		msg.innerHTML = "Please Select any values to search <a class='messageCloseButton' href='#'>Close</a>";
		return false;
	}
	else if(from != 'yy-mm-dd' && to != 'yy-mm-dd')
	{
		if(from > to)
		{
			var msg = document.getElementById("showMessage");
	                msg.style.display = "block";
        	        msg.className = "message error";
			msg.innerHTML = "From date should be greater than To date <a class='messageCloseButton' href='#'>Close</a>";
			return false;			
		}
	}
	else
	{
		return true;
	}
}

</script>

<script type = "text/javascript">

var datepickerDateFormat = 'yy-mm-dd';
$(document).ready(function(){
var rDate = trim($("#todate").val());
if (rDate == '') {
$("#todate").val(datepickerDateFormat);
}
//Bind date picker
daymarker.bindElement("#todate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#todate_Button').click(function(){
daymarker.show("#todate");
});
});

$(document).ready(function(){
var rDate = trim($("#fromdate").val());
if (rDate == '') {
$("#fromdate").val(datepickerDateFormat);
}
//Bind date picker
daymarker.bindElement("#fromdate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#fromdate_Button').click(function(){
daymarker.show("#fromdate");
});
});

</script>

<script type = "text/javascript">
var employees = <?php echo html_entity_decode($emp_tosearch); ?> 
$(document).ready(function() {
var nameField = $("#expenseList_empName");
var idStoreField = $("#expenseList_empId");
var typeHint = 'Type for hints...';
var hintClass = 'inputFormatHint';
var loadingMethod = '';
nameField.one('focus', function() {
if ($(this).hasClass(hintClass)) {
$(this).val("");
$(this).removeClass(hintClass);
}
});
if( loadingMethod != 'ajax'){
if (nameField.val() == '') {
nameField.val(typeHint).addClass(hintClass);
}
nameField.autocomplete(employees, {
formatItem: function(item) {
return item.name;
}
,matchContains:true
}).result(function(event, item) {
idStoreField.val(item.id);
}
);
}else{
nameField.val('Loading').addClass('loading');
$.ajax({
url: "/Expense/symfony/web/index.php/pim/getEmployeeListAjax",
data: "",
dataType: 'json',
success: function(employeeList){
nameField.autocomplete(employeeList, {
formatItem: function(item) {
return item.name;
}
,matchContains:true
}).result(function(event, item) {
idStoreField.val(item.id);
}
);
nameField.css("background-image", "none");
nameField.val(typeHint).addClass(hintClass);
}
});
}
});

</script>
<?php echo javascript_include_tag('orangehrm.datepicker.js'); ?>
<?php
	// Making redirection for search

	$redirect_next = ExpenseDao::searchnextRedirect();
	$redirect_prev = ExpenseDao::searchprevRedirect();

	$expenseTypes = '<select name = "extype" id = "extype" > <option value = "none"> Select </option>';
	foreach($expenseType as $singleExpenseType)
	{
		if($singleExpenseType['expensetypeId'] == $_SESSION['expense_search']['expense_type_id'])	
		{
			$expenseTypes = $expenseTypes."<option selected value = \"".$singleExpenseType['expensetypeId']."\">".$singleExpenseType['expenseName']."</option>";
		}
		else
		{
			$expenseTypes = $expenseTypes."<option value = \"".$singleExpenseType['expensetypeId']."\">".$singleExpenseType['expenseName']."</option>";
		}
	}
	$expenseTypes = $expenseTypes."</select>"; ?>
	<div class = "box toggableForm">
		<form name = "searchExpenseList" onsubmit = "return checkSearch();" action = "#" method = "post" id = "frmExpense">
			<div class = "head">
				<h1> Expense List </h1>
			</div>
			<div class = "inner">
	<div id = "showMessage"> </div>

	<?php $remain = 0; if($_SESSION['smack_message'] == 'update') { $remain = 1; ?>
	<div class="message success"> 
			 Expense List Updated Successfully <a class='messageCloseButton' href='#'>Close</a> <?php  $_SESSION['smack_message'] = ''; ?> 
	</div>  
	<?php if(!empty($_SESSION['expenseWarning']) || !isset($_SESSION['expenseWarning']) ) { ?>
	<div class="message warning"> 
			 <h2> Expense Amount Exceeds for 
				<?php   $c = 0; 
				  	foreach($_SESSION['expenseWarning'] as $showExpense) 
					{
						if($c == 0)
						{
							echo ExpenseDao::getExpenseTypeById($showExpense); $c ++ ;
						}
						else
						{
							echo ', '.ExpenseDao::getExpenseTypeById($showExpense); $c ++;							
						}
					 }
				 ?>
			</h2>
		<?php  $_SESSION['expenseWarning'] = ''; ?> 
	</div>  
	<?php } ?>
<?php } ?>

			<table> 
				<input type = "hidden" name = "search" value = "true">
				<tr height = "35px;"> 
					<input id="expenseList_empId" type="hidden" value="" name="expenseList_empId" <?php if($_SESSION['expense_search']['expenseList_empId']) { ?> value="<?php echo $_SESSION['expense_search']['expenseList_empId']; ?>" <?php } ?>>
					<td width = "25%"> <label> Employee Name </label> </td> 
					<td width = "25%">
						<input id="expenseList_empName" class="ac_input" <?php if($_SESSION['expense_search']['expenseList_empName']) { ?> value="<?php echo $_SESSION['expense_search']['expenseList_empName']; ?>" <?php } ?> name="expenseList_empName" autocomplete="off"> 
					</td>
					<td width = "25%"> <label> Expense Name </label> </td> 
					<td width = "25%"> <?php echo htmlspecialchars_decode($expenseTypes); ?> </td>
				</tr>
				<tr height = "35px;">
					<td width = "25%"> <label> From Date </label> </td>
					<td width = "25%"> 
						<input type = "textbox" readonly name = "fromdate" <?php if($_SESSION['expense_search']['fromdate']) { ?> value="<?php echo $_SESSION['expense_search']['fromdate'] ?>" <?php  } ?> id = "fromdate"> 
					</td> 

					<td width = "25%"> <label> To Date </label> </td>
					<td width = "25%"> 
						<input type = "textbox" readonly name = "todate" id = "todate" <?php if($_SESSION['expense_search']['todate']) { ?> value="<?php echo $_SESSION['expense_search']['todate'] ?>" <?php  } ?> >
					</td> 
				</tr>
				<tr height = "35px;"> 	
				        <td width = "25%"> Expense With status </td>
					<td width = "30%"> <span class = "moveLabel"> Pending </span> <span class = "moveRight">  <input type = "checkbox" name = "pending" id = "pending" <?php if($_SESSION['expense_search']['pending']) { ?> checked <?php  } ?> >  </span>
					<span class = "moveLabel"> Approved </span> <span class = "moveRight"> <input type = "checkbox" name = "approved" id = "approved" <?php if($_SESSION['expense_search']['approved']) { ?> checked <?php  } ?>> </span>
					<span class = "moveLabel"> Cancelled </span> <span class = "moveRight"> <input type = "checkbox" name = "cancelled" <?php if($_SESSION['expense_search']['cancelled']) { ?> checked <?php  } ?> id = "cancelled"> </span>
					</td>
					<td width = "30%">
					<span class = "moveLabel"> Rejected </span>  <input type = "checkbox" name = "rejected" id = "rejected" <?php if($_SESSION['expense_search']['rejected']) { ?> checked <?php  } ?>>
					 </td>
				</tr>
			</table>
			<div class="buttonWrapper">
				<input id="btnSearch" class="searchbutton" type = "submit" value="Search" name="btnSearch" >
				<input id="btnReset" class="clearbutton" type="button" onclick = "clearSearch();" value="Reset" name="btnReset">
				<input id="pageNo" type="hidden" value="<?php echo $_SESSION['smackExpensePage'] ?>" name="pageNo">
			</div>
		</form>
		</div>
	</div>

<form name = "frmExpense" action = "#" method = "post" id = "frmExpense" style = "width:auto;" >
<div id="search-results" class="box noHeader">
<div class="inner">
<div class = "top"> </div>
<div id = "tableWrapper">
       <input type = "hidden" name = "action" value = "save">
       <input type = "hidden" name = "empId" value = "<?php echo ExpenseDao::getEmployeeIdById($_SESSION['user']); ?>" >
       <table id="resultTable" class="table hover"> 
		<tr height = "35px;"> 	
			<th> Employee Name </th> 
			<th> Expense Name </th> 
			<th> Expense Amount </th> 
			<th> Expense Date </th> 
			<th> Status </th> 
			<th> Action </th>
			<?php if($remain == 1) { ?> <th class = "tab_th"> Budget Balance </th> <?php  } ?>
		</tr>
<?php $chkeven = 1; ?>
<?php 
	foreach($values as $single) 
	{	
		$status = ''; 
		foreach($status_expense as $key => $assign_status)
		{
			if($key == $single['expense_status'])
			{
				$status = $status."<option selected value = '{$key}'> {$assign_status} </option>";
			}
			else
			{
				$status = $status."<option value = '{$key}'> {$assign_status} </option>";
			}
		}
		$status = "<select name = 'status' > <option value = '0'> Select Action </option>".$status."</select>";

		$action = '';
		foreach($actions as $key => $split_action)
		{
			$action = $action."<option value = '{$key}'> {$split_action} </option>";
		}
		$action = "<select name = '{$single['expense_type_id']}-{$single['expense_id']}-status' >".$action."</select>"; 

		$after_selection = '';
		foreach($afterselection as $afterselection_key => $afterselection_action)
		{
			$after_selection = $after_selection."<option value = '{$afterselection_key}'> {$afterselection_action} </option>";
		}
		$after_selection = "<select name = '{$single['expense_type_id']}-{$single['expense_id']}-status' >".$after_selection."</select>"; 

		$empId = ExpenseDao::getEmployeeNumberById($single['employee_id']);  ?>

			<tr <?php if($chkeven % 2 == 0) { ?> class="even" <?php } if($chkeven % 2 == 1) { ?> class="odd" <?php } ?> >
			<?php $chkeven++;?>
			<td class = "tab_td"> <a href = "../../index.php/pim/viewPersonalDetails/empNumber/<?php echo $single['employee_id']; ?>" > <?php echo ExpenseDao::getEmployeeNameById($single['employee_id']); ?> </a> </td>
			<td class = "tab_td"> <?php echo ExpenseDao::getExpenseNameById($single['expense_type_id']); ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_amount']; ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_date']; ?> </td>
			<td class = "tab_td"> <?php if($single['expense_status'] == 1) 
			{ 
				echo "Pending"; 
			}
		else if($single['expense_status'] == -1) 
		{
			echo "Rejected"; 
		} 
		else if($single['expense_status'] == 0) 
		{ 
			echo "Cancelled"; 
		} 
		else if($single['expense_status'] == 2) 
		{
			echo "Approved"; 
		} ?> 
		</td>
		<td style = "height:30px;width:150px;"> <?php if($single['expense_status'] == 1) { echo $action; } else { echo $after_selection; } ?>  </td>
		<?php if($remain == 1) { ?>  <td style = "color:red;width:150px;"> <?php echo $single['original_amount']; ?>  </td> <?php } ?>
		</tr>
	<?php }
?>
</table>

	<div class = "bottom"> 
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Save"  class="applybutton" id="saveBtn" >
		<span style = "margin-left:40%;">  
			<?php if($Search_Expense) {   ?>
				<input id="pageno" class="clearbutton" type="button" onclick = "page_search(this.value);" <?php if($_SESSION['smackExpensePage'] == 0) { ?> disabled = "disabled" <?php } ?> value="Previous" name="btnReset">
				<input id="pageno" class="clearbutton" type="button" onclick = "page_search(this.value);" <?php if($next_disable) { ?> disabled = "disabled" <?php } ?>  value="Next" name="btnReset">  
			<?php	} else {  ?>
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($_SESSION['smackExpensePage'] == 0) { ?> disabled = "disabled" <?php } ?> value="Previous" name="btnReset">
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($next_disable) { ?> disabled = "disabled" <?php } ?>  value="Next" name="btnReset">  <?php } ?>
		</span>
	</div>
	</div>
		<script type = "text/javascript">
			function page(val)
			{
				if(val == 'Next')
				{
					window.location.href = 'ExpenseList?page=Next';
				}
				else if(val == 'Previous')
				{
					window.location.href = 'ExpenseList?page=Previous';
				}
			}

			function page_search(val)
			{
				if(val == 'Next')
				{
					window.location.href = "<?php echo $redirect_next; ?>";
				}
				else if(val == 'Previous')
				{
					window.location.href = "<?php echo $redirect_prev; ?>";
				}
			}
		</script>
	</div>
	<div> 
</div>
</div>
</div>
</form>

