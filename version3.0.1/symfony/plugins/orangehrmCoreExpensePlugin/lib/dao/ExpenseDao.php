<?php

class ExpenseDao extends BaseDao {

	public function getExpenseTypeSummary($page)
	{
		try
		{
			if($page == 'Next')
			{
				$_SESSION['smackExpenseTypeSummary'] = $_SESSION['smackExpenseTypeSummary'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpenseTypeSummary'] >= 10)
				{
					$_SESSION['smackExpenseTypeSummary'] = $_SESSION['smackExpenseTypeSummary'] - 10;
				}
			} 

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select expense_type_id,sum(expense_amount) as amount from hs_hr_expense group by expense_type_id limit {$_SESSION['smackExpenseTypeSummary']},10";
			$record = $q->fetchAll($query);

			$query_count = "select expense_type_id,sum(expense_amount) as amount from hs_hr_expense group by expense_type_id";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackExpenseTypeSummary'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getAdminExpenseSummary($page)
	{
		try
		{
			if($page == 'Next')
			{
				$_SESSION['smackExpenseSummary'] = $_SESSION['smackExpenseSummary'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpenseSummary'] >= 10)
				{
					$_SESSION['smackExpenseSummary'] = $_SESSION['smackExpenseSummary'] - 10;
				}
			} 

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select employee_id,sum(expense_amount) as amount from hs_hr_expense group by employee_id limit {$_SESSION['smackExpenseSummary']},10";
			$record = $q->fetchAll($query);

			$query_count = "select employee_id,sum(expense_amount) as amount from hs_hr_expense group by employee_id";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackExpenseSummary'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getExpenseSummary($page,$emp_number)
	{
		try
		{
			if($page == 'Next')
			{
				$_SESSION['smackExpenseSummary'] = $_SESSION['smackExpenseSummary'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpenseSummary'] >= 10)
				{
					$_SESSION['smackExpenseSummary'] = $_SESSION['smackExpenseSummary'] - 10;
				}
			} 

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select employee_id,sum(expense_amount) as amount from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id and active = 1 join ohrm_project_admin c on c.emp_number = {$emp_number} and c.project_id = b.project_id group by employee_id limit {$_SESSION['smackExpenseSummary']},10";
			$record = $q->fetchAll($query);

			$query_count = "select employee_id,sum(expense_amount) as amount from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id and active = 1 join ohrm_project_admin c on c.emp_number = {$emp_number} and c.project_id = b.project_id group by employee_id";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackExpenseSummary'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}


	public function getProjectSummary($page, $emp_number)
	{
		try
		{
			if($page == 'Next')
			{
				$_SESSION['smackProjectSummary'] = $_SESSION['smackProjectSummary'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackProjectSummary'] >= 10)
				{
					$_SESSION['smackProjectSummary'] = $_SESSION['smackProjectSummary'] - 10;
				}
			} 

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select b.project_id, sum(expense_amount) as amount from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id and active = 1 join ohrm_project_admin c on c.emp_number = {$emp_number} and c.project_id = b.project_id group by b.project_id limit {$_SESSION['smackProjectSummary']},10";
			$record = $q->fetchAll($query);

			$query_count = "select b.project_id, sum(expense_amount) as amount from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id and active = 1 join ohrm_project_admin c on c.emp_number = {$emp_number} and c.project_id = b.project_id group by b.project_id";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackProjectSummary'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getAdminProjectSummary($page)
	{
		try
		{
			if($page == 'Next')
			{
				$_SESSION['smackProjectSummary'] = $_SESSION['smackProjectSummary'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackProjectSummary'] >= 10)
				{
					$_SESSION['smackProjectSummary'] = $_SESSION['smackProjectSummary'] - 10;
				}
			} 

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select project_id,sum(expense_amount) as amount from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id and active = 1 group by project_id limit {$_SESSION['smackProjectSummary']},10";
			$record = $q->fetchAll($query);

			$query_count = "select project_id,sum(expense_amount) as amount from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id and active = 1 group by project_id";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackProjectSummary'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}


	public function getAllExpenseType()
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('ExpenseType ele')
			->where('active = ?',1);
			$record = $q->fetchArray();
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getAdminExpense($page)
	{	
		try	
		{
			if($page == 'Next')
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpensePage'] >= 10)
				{
					$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
				}
			} 

			// To get the total count of Expense List and compare it with the current

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on a.expense_type_id = b.expense_type_id join ohrm_project c on b.project_id = c.project_id {$_SESSION['smack_finalorder']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);

			$query_count = "select * from hs_hr_expense a join hs_hr_expense_type b on a.expense_type_id = b.expense_type_id join ohrm_project c on b.project_id = c.project_id";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getExpense($page)
	{	
		try	
		{
			if($page == 'Next')
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpensePage'] >= 10)
				{
					$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
				}
			} 

			// To get the total count of Expense List and compare it with the current

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id join ohrm_project_admin c on c.emp_number = {$_SESSION['empNumber']} and b.project_id = c.project_id {$_SESSION['smack_finalorder']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);

			$query_count = "select * from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id join ohrm_project_admin c on c.emp_number = {$_SESSION['empNumber']} and b.project_id = c.project_id";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function MyExpense($userId, $page)
	{
		try	
		{	
			if($page == 'Next')
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpensePage'] >= 10)
				{
					$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
				}
			} 

			// To get the total count of Expense List and compare it with the current

			$count_rec = Doctrine_Query::create()
			->from('Expense ele')
			->where('employee_id = ?',$userId);
			$count_expenselist = $count_rec->fetchArray();
			$count = count($count_expenselist); 

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			$q = Doctrine_Query::create()
			->from('Expense ele')
			->where('employee_id = ?',$userId)
			->limit(10)
			->offset($_SESSION['smackExpensePage']);
			$record = $q->fetchArray();
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function saveExpense($values, $ExpenseId)
	{
		  try 
		  {	
			$saveProject = new Expense();
			$saveProject->setExpenseId($ExpenseId);
			$saveProject->setExpenseAmount($values['amount']);
			$saveProject->setExpenseTypeId($values['expense']);
			$saveProject->setEmployeeId($values['empId']);
			$saveProject->setExpenseDate($values['expensedate']);
			$saveProject->setExpenseStatus(1);
			$saveProject->setExpenseComments($values['comments']);
			$saveProject->save();
		  	return true;
		   }
		   catch (Exception $e) 
		   {
			    throw new DaoException($e->getMessage());
		   }

	}

	public function updateExpense($values, $id)
	{
		try	
		{	
			foreach($values as $key => $single)
			{
				if($single != 'none')
				{
					$expense_id = explode('-',$key);
					$q = Doctrine_Query::create()
			                    ->update('Expense lt')
			                    ->set('lt.expense_status', '?', $single)
			                    ->where("lt.expense_id = ?", $expense_id[0]);
					$q->execute(); 
				}
			}
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}		
		
	}

	public function checkExpenseBalance($values)
	{
		$_SESSION['expenseWarning'] = array();
		$check = 0;$check_expenseId = 0;
		$make_ExpenseId = array();

		foreach($values as $value => $key)
		{
			if($key == 2)
			{
				$split = explode('-',$value);
				$check_ExpenseId = array_key_exists($split[0], $make_ExpenseId);
				if($check_ExpenseId)
				{
					$make_ExpenseId[$split[0]] = $make_ExpenseId[$split[0]].",".$split[1];
				}
				else
				{
					$make_ExpenseId[$split[0]] = $split[1];
				}
			}
		}
			
		if($make_ExpenseId)
		{	
			foreach($make_ExpenseId as $m_key => $m_val)
			{
				$q = Doctrine_Manager::getInstance()->getCurrentConnection();
				$query = "select sum(expense_amount) as expense_amount from hs_hr_expense where expense_id IN ($m_val)";
				$fetch_all = $q->fetchAll($query);
				$make_Expenseamount[$m_key] = $fetch_all[0]['expense_amount'];
			}
		}

		//	Compare the amount with ExpeseType Amount. If Exceeds Unset the values

		foreach($make_Expenseamount as $key_expamt => $expamt)
		{
			$expense_con = Doctrine_Query::create()
				->from('ExpenseType ele')
				->where('expense_type_id = ?',$key_expamt);
				$expense_type = $expense_con->fetchArray();

			if($expamt > $expense_type[0]['original_amount']) 
			{
				// Unsetting the Expense Type, if the Amount Exceeds

				foreach($values as $value => $key)
				{
					$split_rem = explode('-',$value);
					if($split_rem[0] == $key_expamt)
					{
						// Unsetting the Expense Type, if the Amount Exceeds

						foreach($values as $val => $spl_key)
						{
							$split_unset = explode('-',$val);
							if($split_unset[0] == $split_rem[0])
							{
								unset($values[$val]);
								$checkIn = in_array($split_rem[0],$_SESSION['expenseWarning']);
								if(!$checkIn)
								{	
									$_SESSION['expenseWarning'][] = $split_rem[0];
									$check ++ ;
								}
							}
						}
					}
				}
			}			
		}
		return $values;
	}

	public function updateExpenseList($values)
	{
		try
		{	
			foreach($values as $value => $key)
			{
				if($key != 'none')
				{	
					$split = explode('-',$value);
					$q_update = Doctrine_Query::create()
				             ->update('Expense lt')
					     ->set('lt.expense_status', '?', $key)
					     ->where("lt.expense_id = ?", $split[1]);
					$q_update->execute(); 

					// 	Subtract the amount in Expense Type
					
					if($key == 2)
					{
						$q = Doctrine_Manager::getInstance()->getCurrentConnection();
						$query = "update hs_hr_expense_type a join hs_hr_expense b on b.expense_type_id = a.expense_type_id set original_amount = (budget - expense_amount) where expense_id = {$split[1]} ";
						$fetch_all = $q->execute($query);
					}
				}
			}
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getProjectNameById($id)
	{
		try	
		{
			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			if(isset($id) && !empty($id))
			{
				$query = "select name from ohrm_project where project_id = {$id}";
				$record = $q->fetchAll($query);
				return $record[0]['name'];
			}
			else
			{
				return '-';
			}
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}

	}

	public function getExpenseTypeById($id)
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('ExpenseType exp')
			->where('expense_type_id = ?',$id);
			$record = $q->fetchOne();
			return $record['expenseName'];
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getEmployeeNameById($id)
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('SystemUser usr')
			->where('emp_number = ?',$id);
			$record = $q->fetchOne();
			return $record['user_name'];
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getEmployeeIdById($id)
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('SystemUser usr')
			->where('id = ?',$id);
			$record = $q->fetchOne();
			return $record['emp_number'];
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getEmployeeNumberById($id)
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('SystemUser usr')
			->where('id = ?',$id);
			$record = $q->fetchOne();
			return $record['emp_number'];
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}


	public function SearchMyExpense($values, $id, $page)
	{
		if($page == 'Next')
		{
			$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
		}
		else if($page == 'Previous')
		{
			if($_SESSION['smackExpensePage'] >= 10)
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
			}
		} 

		$status = '';
		if($values['pending'] == 'on') { $status = $status."1,"; }
		if($values['approved'] == 'on') { $status = $status."2,"; }
		if($values['cancelled'] == 'on') { $status = $status."0,"; }
		if($values['rejected'] == 'on') { $status = $status."-1,"; }
		$status = substr($status, 0, -1); 

		try {

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			
			//	Making query to search the Expense List
		
			$query = "select * from hs_hr_expense where employee_id = $id AND ";
			
			if($values['fromdate'] != '' && $values['fromdate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date >= '{$values[fromdate]}' AND "; 
				$_SESSION['expense_search']['fromdate'] = $values['fromdate']; 
			}

			if($values['todate'] != '' && $values['todate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date <= '{$values[todate]}' AND "; 
				$_SESSION['expense_search']['todate'] = $values['todate']; 
			}

			if($values['extype'] != 'none' && $values['extype'] != '') 
			{
				$query = $query."expense_type_id = {$values[extype]} AND "; 
				$_SESSION['expense_search']['expense_type_id'] = $values['extype']; 
			}

			if($values['pending'] == 'on' || $values['approved'] == 'on' || $values['cancelled'] == 'on' || $values['rejected'] == 'on') 
			{
				$query = $query."expense_status IN ({$status}) AND"; 
				$_SESSION['expense_search']['pending'] = $values['pending']; 
				$_SESSION['expense_search']['approved'] = $values['approved']; 
				$_SESSION['expense_search']['cancelled'] = $values['cancelled']; 
				$_SESSION['expense_search']['rejected'] = $values['rejected']; 

			}
			$query = substr($query, 0, -4); 

			// To get the total count of Expense List and compare it with the current

			$fetch_all = $q->fetchAll($query);
			$count = count($fetch_all);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			$query = $query." limit {$_SESSION['smackExpensePage']},10";
			$record = $q->execute($query);
			return $record;
		}
		catch (Exception $e) 
		{
			throw new DaoException( $e->getMessage());
		}
	}

	public function searchnextRedirect()
	{
		$redirect_next = "ExpenseList?search=true&page=Next";
		$redirect_next = $redirect_next."&extype=".$_SESSION['expense_search']['expense_type_id']."&fromdate=".$_SESSION['expense_search']['fromdate']."&todate=".$_SESSION['expense_search']['todate']."&emp_id=".$_SESSION['expense_search']['emp_id']."&pending=".$_SESSION['expense_search']['pending']."&approved=".$_SESSION['expense_search']['approved']."&cancelled=".$_SESSION['expense_search']['cancelled']."&rejected=".$_SESSION['expense_search']['rejected']."&expenseList_empId=".$_SESSION['expense_search']['expenseList_empId']."&=expenseList_empName".$_SESSION['expense_search']['expenseList_empName'];
		return $redirect_next;
	}

	public function searchprevRedirect()
	{
		$redirect_previous = "ExpenseList?search=true&page=Previous";
		$redirect_previous = $redirect_previous."&extype=".$_SESSION['expense_search']['expense_type_id']."&fromdate=".$_SESSION['expense_search']['fromdate']."&todate=".$_SESSION['expense_search']['todate']."&emp_id=".$_SESSION['expense_search']['emp_id']."&pending=".$_SESSION['expense_search']['pending']."&approved=".$_SESSION['expense_search']['approved']."&cancelled=".$_SESSION['expense_search']['cancelled']."&rejected=".$_SESSION['expense_search']['rejected']."&expenseList_empId=".$_SESSION['expense_search']['expenseList_empId']."&=expenseList_empName".$_SESSION['expense_search']['expenseList_empName'];
		return $redirect_previous;
	}

	public function searchExpenseList($values, $page)
	{
		if($page == 'Next')
		{
			$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
		}
		else if($page == 'Previous')
		{
			if($_SESSION['smackExpensePage'] >= 10)
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
			}
		} 

		$status = '';
		if($values['pending'] == 'on') { $status = $status."1,"; }
		if($values['approved'] == 'on') { $status = $status."2,"; }
		if($values['cancelled'] == 'on') { $status = $status."0,"; }
		if($values['rejected'] == 'on') { $status = $status."-1,"; }
		$status = substr($status, 0, -1); 

		try {

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			
			//	Making query to search the Expense List
		
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id join ohrm_project_admin c on c.emp_number = {$_SESSION['empNumber']} and b.project_id = c.project_id where ";

			if($values['expenseList_empId'] != '')
			{
				$query = $query."employee_id = {$values['expenseList_empId']} AND ";
				$_SESSION['expense_search']['expenseList_empId'] = $values['expenseList_empId']; 
				$_SESSION['expense_search']['expenseList_empName'] = $values['expenseList_empName']; 
			}
			
			if($values['fromdate'] != '' && $values['fromdate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date >= '{$values[fromdate]}' AND "; 
				$_SESSION['expense_search']['fromdate'] = $values['fromdate']; 
			}

			if($values['todate'] != '' && $values['todate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date <= '{$values[todate]}' AND "; 
				$_SESSION['expense_search']['todate'] = $values['todate']; 
			}

			if($values['extype'] != 'none' && $values['extype'] != '') 
			{
				$query = $query."b.expense_type_id = {$values[extype]} AND "; 
				$_SESSION['expense_search']['expense_type_id'] = $values['extype']; 
			}

			if($values['pending'] == 'on' || $values['approved'] == 'on' || $values['cancelled'] == 'on' || $values['rejected'] == 'on') 
			{
				$query = $query."expense_status IN ({$status}) AND"; 
				$_SESSION['expense_search']['pending'] = $values['pending']; 
				$_SESSION['expense_search']['approved'] = $values['approved']; 
				$_SESSION['expense_search']['cancelled'] = $values['cancelled']; 
				$_SESSION['expense_search']['rejected'] = $values['rejected']; 

			}
			$query = substr($query, 0, -4);

			// To get the total count of Expense List and compare it with the current

			$fetch_all = $q->fetchAll($query);
			$count = count($fetch_all);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			$query = $query."{$_SESSION['smack_finalorder']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);
			return $record;
		}
		catch (Exception $e) 
		{
			throw new DaoException( $e->getMessage());
		}
	}

	public function searchFinalSummary($values, $page)
	{
		if($page == 'Next')
		{
			$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
		}
		else if($page == 'Previous')
		{
			if($_SESSION['smackExpensePage'] >= 10)
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
			}
		} 

		$status = '';
		if($values['pending'] == 'on') { $status = $status."1,"; }
		if($values['approved'] == 'on') { $status = $status."2,"; }
		if($values['cancelled'] == 'on') { $status = $status."0,"; }
		if($values['rejected'] == 'on') { $status = $status."-1,"; }
		$status = substr($status, 0, -1); 

		try {

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			
			//	Making query to search the Expense List
		
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id join ohrm_project_admin c on c.emp_number = {$_SESSION['empNumber']} and b.project_id = c.project_id where ";

			if($values['expenseList_empId'] != '')
			{
				$query = $query."employee_id = {$values['expenseList_empId']} AND ";
				$_SESSION['expense_search']['expenseList_empId'] = $values['expenseList_empId']; 
				$_SESSION['expense_search']['expenseList_empName'] = $values['expenseList_empName']; 
			}
			
			if($values['fromdate'] != '' && $values['fromdate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date >= '{$values[fromdate]}' AND "; 
				$_SESSION['expense_search']['fromdate'] = $values['fromdate']; 
			}

			if($values['todate'] != '' && $values['todate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date <= '{$values[todate]}' AND "; 
				$_SESSION['expense_search']['todate'] = $values['todate']; 
			}

			if($values['project'] != 'none') 
			{
				$query = $query."c.project_id = '{$values[project]}' AND "; 
				$_SESSION['expense_search']['project_id'] = $values['project']; 
			}

			if($values['extype'] != 'none' && $values['extype'] != '') 
			{
				$query = $query."b.expense_type_id = {$values[extype]} AND "; 
				$_SESSION['expense_search']['expense_type_id'] = $values['extype']; 
			}

			if($values['pending'] == 'on' || $values['approved'] == 'on' || $values['cancelled'] == 'on' || $values['rejected'] == 'on') 
			{
				$query = $query."expense_status IN ({$status}) AND"; 
				$_SESSION['expense_search']['pending'] = $values['pending']; 
				$_SESSION['expense_search']['approved'] = $values['approved']; 
				$_SESSION['expense_search']['cancelled'] = $values['cancelled']; 
				$_SESSION['expense_search']['rejected'] = $values['rejected']; 

			}
			$query = substr($query, 0, -4);

			// To get the total count of Expense List and compare it with the current

			$fetch_all = $q->fetchAll($query);
			$count = count($fetch_all);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			$query = $query."{$_SESSION['smack_finalorder']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);
			return $record;
		}
		catch (Exception $e) 
		{
			throw new DaoException( $e->getMessage());
		}
	}


	public function searchAdminFinalSummary($values, $page)
	{
		if($page == 'Next')
		{
			$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
		}
		else if($page == 'Previous')
		{
			if($_SESSION['smackExpensePage'] >= 10)
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
			}
		} 

		$status = '';
		if($values['pending'] == 'on') { $status = $status."1,"; }
		if($values['approved'] == 'on') { $status = $status."2,"; }
		if($values['cancelled'] == 'on') { $status = $status."0,"; }
		if($values['rejected'] == 'on') { $status = $status."-1,"; }
		$status = substr($status, 0, -1); 

		try {

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			
			//	Making query to search the Expense List
		
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on a.expense_type_id = b.expense_type_id join ohrm_project c on b.project_id = c.project_id where ";

			if($values['expenseList_empId'] != '')
			{
				$query = $query."employee_id = {$values['expenseList_empId']} AND ";
				$_SESSION['expense_search']['expenseList_empId'] = $values['expenseList_empId']; 
				$_SESSION['expense_search']['expenseList_empName'] = $values['expenseList_empName']; 
			}
			
			if($values['fromdate'] != '' && $values['fromdate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date >= '{$values[fromdate]}' AND "; 
				$_SESSION['expense_search']['fromdate'] = $values['fromdate']; 
			}

			if($values['project'] != 'none') 
			{
				$query = $query."c.project_id = '{$values[project]}' AND "; 
				$_SESSION['expense_search']['project_id'] = $values['project']; 
			}

			if($values['todate'] != '' && $values['todate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date <= '{$values[todate]}' AND "; 
				$_SESSION['expense_search']['todate'] = $values['todate']; 
			}

			if($values['extype'] != 'none' && $values['extype'] != '') 
			{
				$query = $query."a.expense_type_id = {$values[extype]} AND "; 
				$_SESSION['expense_search']['expense_type_id'] = $values['extype']; 
			}

			if($values['pending'] == 'on' || $values['approved'] == 'on' || $values['cancelled'] == 'on' || $values['rejected'] == 'on') 
			{
				$query = $query."expense_status IN ({$status}) AND"; 
				$_SESSION['expense_search']['pending'] = $values['pending']; 
				$_SESSION['expense_search']['approved'] = $values['approved']; 
				$_SESSION['expense_search']['cancelled'] = $values['cancelled']; 
				$_SESSION['expense_search']['rejected'] = $values['rejected']; 

			}
			$query = substr($query, 0, -4);

			// To get the total count of Expense List and compare it with the current

			$fetch_all = $q->fetchAll($query);

			$count = count($fetch_all);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			$query = $query."{$_SESSION['smack_finalorder']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);
			return $record;
		}
		catch (Exception $e) 
		{
			throw new DaoException( $e->getMessage());
		}
	}



	public function searchAdminExpenseList($values, $page)
	{
		if($page == 'Next')
		{
			$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
		}
		else if($page == 'Previous')
		{
			if($_SESSION['smackExpensePage'] >= 10)
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
			}
		} 

		$status = '';
		if($values['pending'] == 'on') { $status = $status."1,"; }
		if($values['approved'] == 'on') { $status = $status."2,"; }
		if($values['cancelled'] == 'on') { $status = $status."0,"; }
		if($values['rejected'] == 'on') { $status = $status."-1,"; }
		$status = substr($status, 0, -1); 

		try {

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			
			//	Making query to search the Expense List
		
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on a.expense_type_id = b.expense_type_id join ohrm_project c on b.project_id = c.project_id where ";

			if($values['expenseList_empId'] != '')
			{
				$query = $query."employee_id = {$values['expenseList_empId']} AND ";
				$_SESSION['expense_search']['expenseList_empId'] = $values['expenseList_empId']; 
				$_SESSION['expense_search']['expenseList_empName'] = $values['expenseList_empName']; 
			}
			
			if($values['fromdate'] != '' && $values['fromdate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date >= '{$values[fromdate]}' AND "; 
				$_SESSION['expense_search']['fromdate'] = $values['fromdate']; 
			}

			/*if($values['project'] != 'none') 
			{
				$query = $query."c.project_id = '{$values[project]}' AND "; 
				$_SESSION['expense_search']['project_id'] = $values['project']; 
			}*/

			if($values['todate'] != '' && $values['todate'] != 'yy-mm-dd') 
			{
				$query = $query."expense_date <= '{$values[todate]}' AND "; 
				$_SESSION['expense_search']['todate'] = $values['todate']; 
			}

			if($values['extype'] != 'none' && $values['extype'] != '') 
			{
				$query = $query."a.expense_type_id = {$values[extype]} AND "; 
				$_SESSION['expense_search']['expense_type_id'] = $values['extype']; 
			}

			if($values['pending'] == 'on' || $values['approved'] == 'on' || $values['cancelled'] == 'on' || $values['rejected'] == 'on') 
			{
				$query = $query."expense_status IN ({$status}) AND"; 
				$_SESSION['expense_search']['pending'] = $values['pending']; 
				$_SESSION['expense_search']['approved'] = $values['approved']; 
				$_SESSION['expense_search']['cancelled'] = $values['cancelled']; 
				$_SESSION['expense_search']['rejected'] = $values['rejected']; 

			}
			$query = substr($query, 0, -4);

			// To get the total count of Expense List and compare it with the current

			$fetch_all = $q->fetchAll($query);

			$count = count($fetch_all);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			$query = $query."{$_SESSION['smack_finalorder']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);
			return $record;
		}
		catch (Exception $e) 
		{
			throw new DaoException( $e->getMessage());
		}
	}


	function makeEmployeesarray()
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('Employee ele');
			$record = $q->fetchArray();
			$emp = "[";
			foreach($record as $key => $val) { 
				$emp = $emp."{\"name\":\"$val[firstName]"." "."$val[lastName]\",\"id\":\"$val[empNumber]\"},";
			}
			$emp = substr($emp,0,-1);
			$emp = $emp."]";
			return $emp;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getAutomation()
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('ExpenseAutomation ele')
			->where('id = 1');
			$record = $q->fetchOne();
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
			
	}

	public function updateAutomation($duration)
	{
		try	
		{
			$q = Doctrine_Query::create()
	                    ->update('ExpenseAutomation lt')
	                    ->set('lt.duration', '?', $duration)
	                    ->where("lt.id = 1");
			$q->execute(); 
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
			
	}

	public function getExpenseNameById($id)
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('ExpenseType ele')
			->where('expensetypeId = ?',$id);
			$record = $q->fetchOne();
			return $record['expenseName'];
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getAdminFinalSummary($page)
	{	
		try	
		{
			if($page == 'Next')
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpensePage'] >= 10)
				{
					$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
				}
			} 

			// To get the total count of Expense List and compare it with the current

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on a.expense_type_id = b.expense_type_id join ohrm_project c on b.project_id = c.project_id order by {$_SESSION['smack_ord_order']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);

			$query_count = "select * from hs_hr_expense a join hs_hr_expense_type b on a.expense_type_id = b.expense_type_id join ohrm_project c on b.project_id = c.project_id order by {$_SESSION['smack_ord_order']}";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);


			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	public function getFinalSummary($page)
	{	
		try	
		{
			if($page == 'Next')
			{
				$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] + 10;
			}
			else if($page == 'Previous')
			{
				if($_SESSION['smackExpensePage'] >= 10)
				{
					$_SESSION['smackExpensePage'] = $_SESSION['smackExpensePage'] - 10;
				}
			} 

			// To get the total count of Expense List and compare it with the current

			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select * from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id join ohrm_project_admin c on c.emp_number = {$_SESSION['empNumber']} and b.project_id = c.project_id order by {$_SESSION['smack_ord_order']} limit {$_SESSION['smackExpensePage']},10";
			$record = $q->fetchAll($query);

			$query_count = "select * from hs_hr_expense a join hs_hr_expense_type b on b.expense_type_id = a.expense_type_id join ohrm_project_admin c on c.emp_number = {$_SESSION['empNumber']} and b.project_id = c.project_id order by {$_SESSION['smack_ord_order']}";
			$record_count = $q->fetchAll($query_count);
			$count = count($record_count);

			if($_SESSION['smackExpensePage'] + 10 >= $count)
			{		
				$this->next_disable = true;
			}

			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}
}
