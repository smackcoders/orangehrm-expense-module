<?php
require_once ('lib/confs/Conf.php');
$confobj = new Conf();
$dbhost = $confobj->dbhost;
$dbport = $confobj->dbport;
$dbname = $confobj->dbname;
$dbuser = $confobj->dbuser;
$dbpass = $confobj->dbpass;
$version = $confobj->version; 

$con = mysql_connect($dbhost,$dbuser,$dbpass);
if(!$con)
{
	die('Cant connect to Mysql');
}

$db_check = mysql_select_db($dbname,$con);
if(!$db_check)
{
	die('Database Error');
}

$create_expense_type = "CREATE TABLE `hs_hr_expense_type` (
  `expense_type_id` varchar(10) NOT NULL,
  `expense_type_name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `budget` varchar(10) NOT NULL,
  `active` int(1) NOT NULL,
  `automation` int(1) DEFAULT '1',
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `original_amount` varchar(10) DEFAULT NULL,
  `project_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$create_expense	   = "CREATE TABLE `hs_hr_expense` (
  `expense_id` int(11) NOT NULL,
  `expense_date` date DEFAULT NULL,
  `expense_amount` decimal(11,2) unsigned DEFAULT NULL,
  `expense_status` smallint(6) DEFAULT NULL,
  `expense_comments` varchar(256) DEFAULT NULL,
  `expense_type_id` varchar(13) NOT NULL DEFAULT '',
  `employee_id` int(7) NOT NULL DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`expense_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ";

$create_automation = "CREATE TABLE `hs_hr_expense_automation` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `duration` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1";

$query_expense 		  = "insert into hs_hr_unique_id (last_id,table_name,field_name) values(0,'hs_hr_expense','expense_id')";
$query_expense_type 	  = "insert into hs_hr_unique_id (last_id,table_name,field_name) values(0,'hs_hr_expense_type','expense')";
$query_automation_val 	  = "insert into hs_hr_expense_automation values(1,'none')";
$run_create_expense 	  = mysql_query($create_expense,$con);
$run_create_expense_type  = mysql_query($create_expense_type,$con);
$run_automation 	  = mysql_query($create_automation,$con);
$run_expense 		  = mysql_query($query_expense,$con);
$run_expense_type 	  = mysql_query($query_expense_type,$con);
$query_automation_val	  = mysql_query($query_automation_val,$con);

# Adding Expense Module
$addModuleQuery = "insert into ohrm_module (name) values ('expense')";
$run_moduleQuery = mysql_query($addModuleQuery, $con);

# Get Expense Module Id
$expenseId = getCol('id', 'ohrm_module', 'name', 'expense');

# Adding screen 
mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('Expense', $expenseId, 'showExpenseType')",$con);
mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('Expense Type', $expenseId, 'showExpenseType')",$con);
mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('Expense List', $expenseId, 'ExpenseList')",$con);
mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('Expense Summary', $expenseId, 'FinalSummary')",$con);
mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('Add Expense Type', $expenseId, 'defineExpenseType')", $con);
mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('Expense Automation', $expenseId, 'ExpenseAutomationSetting')",$con);

mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('My Expense', $expenseId, 'myExpense')",$con);
mysql_query("insert into ohrm_screen (name, module_id, action_url) values ('Apply Expense', $expenseId, 'applyExpense')",$con);

$menu_array = array("1" => array("menu_title" => "Expense", "level" => 1, "order" => 800, "parent" => ""),
		    "2" => array("menu_title" => "Expense Type", "screen_id" => "No", "level" => 2, "order" => 100, "parent" => "Expense"),
		    "3" => array("menu_title" => "Add Expense Type", "level" => 3, "order" => 100, "parent" => "Expense Type"),
		    "4" => array("menu_title" => "Expense List", "level" => 2, "order" => 200, "parent" => "Expense"),
		    "5" => array("menu_title" => "Expense Summary", "level" => 2, "order" => 300, "parent" => "Expense"),
		    "6" => array("menu_title" => "My Expense", "level" => 1, "order" => 900, "parent" => ""),
		    "7" => array("menu_title" => "Apply Expense", "level" => 2, "order" => 100, "parent" => "My Expense"),
		    "8" => array("menu_title" => "Expense Automation", "level" => 2, "order" => 400, "parent" => "Expense"),
		);

addMenu($menu_array);

if($run_expense && $run_expense_type && $run_create_expense_type && $run_create_expense && $run_automation && $query_automation_val)
{
	die("<h3 style = 'color:green'> Tables created for Expense module and values were inserted into the table. </h3> <h3 style = 'color:red'> NOTE: We strongly recommend you to remove this file. This script should run once </h3>");
}
else
{
	die("<h3 style = 'color:red'> Error occured. Tables may already exist. </h3>");
}

function addMenu($arr)
{
        $confobj = new Conf();
        $dbhost = $confobj->dbhost;
        $dbport = $confobj->dbport;
        $dbname = $confobj->dbname;
        $dbuser = $confobj->dbuser;
        $dbpass = $confobj->dbpass;
        $version = $confobj->version;

        $con = mysql_connect($dbhost,$dbuser,$dbpass);
        if(!$con)
        {
                die('Cant connect to Mysql');
        }

        $db_check = mysql_select_db($dbname,$con);
        if(!$db_check)
        {
                die('Database Error');
        }

	foreach($arr as $sin_arr)
	{
		$MenuId = NULL;
		if($sin_arr['screen_id'] == "No")	{

		}
		else	{
			$MenuId = getCol("id", "ohrm_screen", "name", "{$sin_arr['menu_title']}");
		}
		if($sin_arr['level'] == 1)
		{
			$parent_id = "";
		}
		else
		{
			$parent_id = getCol("id", "ohrm_menu_item", "menu_title", "{$sin_arr['parent']}"); 
		}

		if($sin_arr['screen_id'] == 'No')
		{
			$query_in = "";
			$query_in = "insert into ohrm_menu_item (menu_title, parent_id, level, order_hint) values ('{$sin_arr['menu_title']}',  '{$parent_id}', {$sin_arr['level']}, {$sin_arr['order']})";
		}
		else
		{
			$query_in = "";
                        $query_in = "insert into ohrm_menu_item (menu_title, screen_id, parent_id, level, order_hint) values ('{$sin_arr['menu_title']}', {$MenuId}, '{$parent_id}', {$sin_arr['level']}, {$sin_arr['order']})";
		}

		$op_in = mysql_query($query_in, $con);

		# Giving Menu Permission for roles
		if($sin_arr['menu_title'] != 'My Expense' && $sin_arr['menu_title'] != 'Apply Expense')
		{
			mysql_query("insert into ohrm_user_role_screen (user_role_id, screen_id, can_read, can_create, can_update, can_delete) values (1, '{$MenuId}', 1, 1, 1, 1)");
		}
		else
		{
			mysql_query("insert into ohrm_user_role_screen (user_role_id, screen_id, can_read, can_create, can_update, can_delete) values (2, '{$MenuId}', 1, 1, 1, 1)");
			mysql_query("insert into ohrm_user_role_screen (user_role_id, screen_id, can_read, can_create, can_update, can_delete) values (3, '{$MenuId}', 1, 1, 1, 1)");
			mysql_query("insert into ohrm_user_role_screen (user_role_id, screen_id, can_read, can_create, can_update, can_delete) values (4, '{$MenuId}', 1, 1, 1, 1)");
			mysql_query("insert into ohrm_user_role_screen (user_role_id, screen_id, can_read, can_create, can_update, can_delete) values (5, '{$MenuId}', 1, 1, 1, 1)");
			mysql_query("insert into ohrm_user_role_screen (user_role_id, screen_id, can_read, can_create, can_update, can_delete) values (6, '{$MenuId}', 1, 1, 1, 1)");
			mysql_query("insert into ohrm_user_role_screen (user_role_id, screen_id, can_read, can_create, can_update, can_delete) values (7, '{$MenuId}', 1, 1, 1, 1)");
		}
	}
}

function getCol($colname, $tablename, $searchcol, $searchval)
{
	$confobj = new Conf();
	$dbhost = $confobj->dbhost;
	$dbport = $confobj->dbport;
	$dbname = $confobj->dbname;
	$dbuser = $confobj->dbuser;
	$dbpass = $confobj->dbpass;
	$version = $confobj->version;

	$con = mysql_connect($dbhost,$dbuser,$dbpass);
	if(!$con)
	{
		die('Cant connect to Mysql');
	}

	$db_check = mysql_select_db($dbname,$con);
	if(!$db_check)
	{
		die('Database Error');
	}

	$query_run = "select $colname from $tablename where $searchcol = '$searchval'";
	$query = mysql_query($query_run, $con);
	$getRes = mysql_fetch_array($query);
	if($getRes)	{
		return $getRes[$colname];
	}
	return false;
}
