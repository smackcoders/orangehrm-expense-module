Intructions to install the expense module:
------------------------------------------
1. Unzip the file from the Orangehrm root directory.
2. Run the expense_install.php file from the browser (eg: http://localhost/orangehrm/expense_install.php)
3. Add the module name 'expense', in symfony/apps/orangehrm/config/settings.yml file

Orangehrm code: 

	enabled_modules:  [default, core, leave, pim, time, attendance, recruitment, recruitmentApply, admin]

You have to change the above line like this: 

	enabled_modules:  [default, core, expense, leave, pim, time, attendance, recruitment, recruitmentApply, admin]

4. Remove the cache ( Cache Location: ./symfony/cache/ )


