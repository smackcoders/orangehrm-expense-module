Intructions to install the expense module:
------------------------------------------

1. Unzip the zip file from the orangehrm root directory.
------------------------------------------------------------------------------------------------------------------------------------
2. Run the expense_install.php from the browser. ( eg: http://localhost/orangehrm/expense_install.php )
------------------------------------------------------------------------------------------------------------------------------------
3. Add the module name 'expense', in symfony/apps/orangehrm/config/settings.yml file

Orangehrm code: 

	enabled_modules:  [default, core, leave, pim, time, attendance, recruitment, recruitmentApply, admin]

You have to change the above line like this: 

	enabled_modules:  [default, core, expense, leave, pim, time, attendance, recruitment, recruitmentApply, admin]

------------------------------------------------------------------------------------------------------------------------------------

4. Add the code given below in index.php before the leave menu 

// Expense menu starts 

if (($_SESSION['empID'] != null) || $arrAllRights[Leave]['view']) {
	$menuItem = new MenuItem("expense", 'Expense' ,"#");
	$menuItem->setCurrent($_GET['menu_no_top']=="expense");

	$subs = array();
	$subsubs = array();

	if($authorizeObj->isAdmin()) 
	{
		$subs[] = new MenuItem("showproject", 'Expense Type', "./symfony/web/index.php/expense/showExpenseType", 'rightMenu');
		$subs[] = new MenuItem("expenseAutomation", 'Expense Automation', "./symfony/web/index.php/expense/ExpenseAutomationSetting", 'rightMenu');
	}

	if(($_SESSION['isProjectAdmin']) || $authorizeObj->isAdmin()) 
	{
		$subs[] = new MenuItem("ExpenseList", 'Expense List', "./symfony/web/index.php/expense/ExpenseList?smack=First", 'rightMenu');
		$sub = new MenuItem("Report", 'Report', "#");
		$subsubs[] = new MenuItem("ExpenseSummary", 'Expense Summary', "./symfony/web/index.php/expense/FinalSummary?smack=First", 'rightMenu');
	        $sub->setSubMenuItems($subsubs);
	        $subs[] = $sub;
	}

	if ($authorizeObj->isESS() && !$authorizeObj->isAdmin()) 
	{
  		$subs[] = new MenuItem("Expenselist", 'My Expense', './symfony/web/index.php/expense/myExpense?smack=First', 'rightMenu');
  		$subs[] = new MenuItem("applyExpense", 'Apply', "./symfony/web/index.php/expense/applyExpense",'rightMenu');
	}	
      
	$menuItem->setSubMenuItems($subs);
	$menu[] = $menuItem;
}

-----------------------------------------------------------------------------------------------------------------------
5. Replace the class file in menu.css [ location: themes/orange/menu/menu.css ] 
-----------------------------------------------------------------------------------------------------------------------

Search for the class name "#nav li.l2 a" in menu.css in the location given. Default width will be 10em. Change the value into 16em. 

Orangehrm Code

	#nav li.l2 a {
		width: 10em;
	}

You have to change the css class like this: 

#nav li.l2 a {
	width: 16em;
}

6. Remove the cache


