<?php
require_once ('lib/confs/Conf.php');
$confobj = new Conf();
$dbhost = $confobj->dbhost;
$dbport = $confobj->dbport;
$dbname = $confobj->dbname;
$dbuser = $confobj->dbuser;
$dbpass = $confobj->dbpass;
$version = $confobj->version; 

$con = mysql_connect($dbhost,$dbuser,$dbpass);
if(!$con)
{
	die('Cant connect to Mysql');
}

$db_check = mysql_select_db($dbname,$con);
if(!$db_check)
{
	die('Database Error');
}

$create_expense_type = "CREATE TABLE `hs_hr_expense_type` (
  `expense_type_id` varchar(10) NOT NULL,
  `expense_type_name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `budget` varchar(10) NOT NULL,
  `active` int(1) NOT NULL,
  `automation` int(1) DEFAULT '1',
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `original_amount` varchar(10) DEFAULT NULL,
  `project_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$create_expense	   = "CREATE TABLE `hs_hr_expense` (
  `expense_id` int(11) NOT NULL,
  `expense_date` date DEFAULT NULL,
  `expense_amount` decimal(11,2) unsigned DEFAULT NULL,
  `expense_status` smallint(6) DEFAULT NULL,
  `expense_comments` varchar(256) DEFAULT NULL,
  `expense_type_id` varchar(13) NOT NULL DEFAULT '',
  `employee_id` int(7) NOT NULL DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`expense_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ";

$create_automation = "CREATE TABLE `hs_hr_expense_automation` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `duration` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1";

$query_expense 		  = "insert into hs_hr_unique_id (last_id,table_name,field_name) values(0,'hs_hr_expense','expense_id')";
$query_expense_type 	  = "insert into hs_hr_unique_id (last_id,table_name,field_name) values(0,'hs_hr_expense_type','expense')";
$query_automation_val 	  = "insert into hs_hr_expense_automation values(1,'none')";
$run_create_expense 	  = mysql_query($create_expense,$con);
$run_create_expense_type  = mysql_query($create_expense_type,$con);
$run_automation 	  = mysql_query($create_automation,$con);
$run_expense 		  = mysql_query($query_expense,$con);
$run_expense_type 	  = mysql_query($query_expense_type,$con);
$query_automation_val	  = mysql_query($query_automation_val,$con);

	if($run_expense && $run_expense_type && $run_create_expense_type && $run_create_expense && $run_automation && $query_automation_val)
	{
		die("<h3 style = 'color:green'> Tables created for Expense module and values were inserted into the table. </h3> <h3 style = 'color:red'> NOTE: We strongly recommend you to remove this file. This script should run once </h3>");
	}
	else
	{
		die("<h3 style = 'color:red'> Error occured. Tables may already exist. </h3>");
	}

