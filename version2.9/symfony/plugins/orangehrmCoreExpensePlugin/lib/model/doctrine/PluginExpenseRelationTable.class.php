<?php

/**
 * PluginExpenseRelationTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginExpenseRelationTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginExpenseRelationTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginExpenseRelation');
    }
}