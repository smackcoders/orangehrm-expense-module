<?php

class ExpenseTypeDao extends BaseDao {

	/*
	 *	Function returns all the Project Names
	 */

	public function getAllProjects()
	{
		try	
		{
			$qa = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select project_id, name from ohrm_project where is_deleted = 0";
			$record = $qa->fetchAll($query);
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	/*
	 *	Function returns  the Project Names
	 */

	public function getEmpProjects($id)
	{
		try	
		{
			$qa = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select * from ohrm_project a join ohrm_project_admin b on a.project_id = b.project_id and emp_number = {$id} and is_deleted = 0";
			$record = $qa->fetchAll($query);
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}


	/*
	 * 	Function returns All the User Information
	 */

	public function getAllUsers()
	{ 
		try	
		{
			$q = Doctrine_Query::create()
			->from('SystemUser ele');
			$record = $q->fetchArray();
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}

	/*	@param Tablename
	 *	Function gets the last id from unique table
	 */

	public function getUniqueId_Smack($tablename)
	{ 
		try
		{
			$q = Doctrine_Query::create()
		                   ->from('UniqueId lt')
		                   ->where("lt.dbTable = ?", $tablename);
			$fetch =  $q->fetchOne();
			$cur_expenseId = $fetch->_data['last_id'] + 1;
			return $cur_expenseId;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}

	}

	/*
	 *	@param Tablename, LastId
	 *	Function gets the last id from unique table
	 */

	public function setUniqueId_Smack($tablename, $lastID)
	{
		try 
		{ 
		    $update_query = Doctrine_Query::create()
		                    ->update('UniqueId lt')
		                    ->set('lt.last_id', $lastID)
		                    ->where("lt.dbTable = ?", $tablename);

		    $numUpdated = $update_query->execute();
		}
		   catch (Exception $e) 
		   {
			    throw new DaoException($e->getMessage());
		   }


	}

	/*
	 *	@param values to save and expenseId	
	 *	Function saves the Expense project
	*/

	public function saveExpense($values,$expenseID)
	{
		  try 
		  {	
			if($values['automation'] == 'on')
			{
				$values['automation'] = 1;
			}
			else
			{
				$values['automation'] = 0;
			}

			$date = date('Y-m-d');
			$saveProject = new ExpenseType();
			$saveProject->setExpenseName($values['name']);
			$saveProject->setExpensetypeId($expenseID);
			$saveProject->setProjectId($values['project']);
			$saveProject->setDescription($values['desc']);
			$saveProject->setActive(1);
			$saveProject->setBudget($values['budget']);
			$saveProject->setOriginalAmount($values['budget']);
			$saveProject->setDateCreated($date);
			$saveProject->setDateUpdated($date);
			$saveProject->setAutomation($values['automation']);
			$saveProject->save();

		  	return true;
		   }
		   catch (Exception $e) 
		   {
			    throw new DaoException($e->getMessage());
		   }
	}

	/*
	 *	Function Update the Expense Amount
	 *	Params expense_id
  	 */

	public function updateExpenseAutomation($expense_id)
	{
		$date = date('Y-m-d');
	        $q = Doctrine_Manager::getInstance()->getCurrentConnection();
		$query_amount = "select budget from hs_hr_expense_type where expense_type_id = {$expense_id}";
		$res = $q->fetchAll($query_amount);

		$qa = Doctrine_Manager::getInstance()->getCurrentConnection();
 		$query = "update hs_hr_expense_type set original_amount = '{$res[0]['budget']}', date_updated = '{$date}' where expense_type_id = {$expense_id}";
		$res = $qa->execute($query);
	}

	/*
	 *	Function Subtract the date_updated with the current date
	 *	Params expense_id
  	 */

	public function diff_date($expense_id)
	{
	        $q = Doctrine_Manager::getInstance()->getCurrentConnection();
 		$query_duration = "select duration from hs_hr_expense_automation where id = 1";
		$res_duration = $q->fetchAll($query_duration);

		$query = "select date_updated, date_sub(curdate(), INTERVAL {$res_duration[0]['duration']} MONTH) as date from hs_hr_expense_type where expense_type_id = {$expense_id}";
		$res = $q->fetchAll($query);
		if($res[0]['date_updated'] < $res[0]['date'])
		{
			return $res[0]['date'];
		}
		else
		{
			return 'none';
		}
	}

	/*	
	 *	Function returns active ExpenseType
	 */

	public function getExpenseType()
	{
		try	
		{
			$q = Doctrine_Query::create()
			->from('ExpenseType ele')
	                ->where("ele.active = 1");
			$record = $q->fetchArray();
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}

	}

	public function getEmpExpenseType($id)
	{
		try	
		{
			$qa = Doctrine_Manager::getInstance()->getCurrentConnection();
			$query = "select * from hs_hr_expense_type a join ohrm_project b on a.project_id = b.project_id join ohrm_project_admin c on b.project_id = c.project_id and emp_number = {$id} and is_deleted = 0";
			$record = $qa->fetchAll($query);
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}
	}


	/*
	 *	@param array of ExpenseId	
	 *	Function Delete ExpenseType
	 */

	function deleteExpense($ids)
	{
		try 
		{
			    $expensetype = Doctrine_Query::create()
				            ->update('ExpenseType lt')
				            ->set('lt.active', '?', '0')
				            ->whereIn('lt.expensetypeId', $ids);
			    $numDeleted = $expensetype->execute();

			    //	Deleting Relations of ExpenseType in ExpenseRelation

			    $q = Doctrine_Manager::getInstance()->getCurrentConnection();
			    foreach($ids as $id)
			    {
				$query = "delete from hs_hr_expense_relation where expense_id = {$id}";
				$q->execute($query);
			    }
			    return true;
		   }
		   catch (Exception $e) 
		   {
			    $this->getLogger()->error("Exception in deleting project:" . $e);
			    throw new DaoException($e->getMessage());
		   }
	}


	/*
	 *	@param values for columns
	 *	Function update ExpenseType
	 */

	public function updateExpense($values)
	{
		try
		{ 
			$date = date('Y-m-d');

			if($values['automation'] == 'on')
			{
				$values['automation'] = 1;
			}
			else
			{
				$values['automation'] = 0;
			}

			//	Check wheather user change the amount for the Expense Type

			$q = Doctrine_Query::create()
			->from('ExpenseType ele')
	                ->where("ele.expensetypeId = ?", $values['expenseid']);
			$record = $q->fetchArray();

			$q_update = Doctrine_Query::create()
	                     ->update('ExpenseType lt')
		             ->set('lt.expenseName', '?', $values['name'])
		             ->set('lt.projectId', '?', $values['project'])
		             ->set('lt.description', '?', $values['desc'])
		             ->set('lt.budget', '?', $values['budget'])
	  	  	     ->set('lt.original_amount', '?', $values['budget']);

		        $q_update->set('lt.automation', '?', $values['automation'])
		                 ->set('lt.date_updated', '?', $date)
		                 ->where("lt.expensetypeId = ?", $values['expenseid']);

			$q_update->execute(); 
			return true;
		}
  	        catch (Exception $e) 
		{
		      throw new DaoException($e->getMessage());
		}
	}

	/*		
	 *	Function Return true if expense present else false
	 *	@param ExpenseName and projectId
	 */

	public function checkExpenseName($name, $id)
	{		
		try	
		{ 
			$q = Doctrine_Manager::getInstance()->getCurrentConnection();
	 		$query = "select expense_type_id from hs_hr_expense_type where expense_type_name = '{$name}' and project_id = '{$id}'";
			$record = $q->fetchAll($query);

			if($record)
			{
				$check = 2;
			}
			else
			{
				$check = 1;
			} 
			return $check;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}

	}

	/*
	 *	@param ExpenseTypeId	
	 *	Function Gets ExpenseType Values 
	 */

	public function getExpenseValue($id)
	{		
		try	
		{ 
			$q = Doctrine_Query::create()
			->from('ExpenseType ele')
                        ->where("ele.expense_type_id = ? ", $id);
			$record = $q->fetchOne(); 
			return $record;
		}
		catch (Exception $e) 
		{
		    throw new DaoException($e->getMessage());
		}

	}

}
