<?php

class editExpenseTypeAction extends orangehrmAction 
{
	    protected $editExpenseService;

	    public function execute($request) 
	    {
		if($_POST['action'] == 'update')
		{
			$this->updateExpense = ExpenseTypeDao::updateExpense($_POST);
			if($this->updateExpense == true)
			{
				$_SESSION['messsage'] = 'update';
				$this->redirect('expense/showExpenseType');
			}
		}

		if(isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] == 'Yes') 
		{
			// Getting id of the project


			$split_uri = explode('/',$_SERVER['REQUEST_URI']);
			$this->id = array_pop($split_uri);
			$this->values = ExpenseTypeDao::getExpenseValue($this->id);
			$this->Projects = ExpenseTypeDao::getAllProjects();
			$this->Project = "";

			foreach($this->Projects as $project)
			{ 	
				if($project['project_id'] == $this->values['projectId'])
				{
					$this->Project = $this->Project."<option value = \"".$project['project_id']."\" selected>".$project['name']."</option>"; 
				}
				else
				{
					$this->Project = $this->Project."<option value = \"".$project['project_id']."\">".$project['name']."</option>"; 
				}
			}
		}
	}
}


