<?php

class ExpenseAutomationSettingAction extends orangehrmAction 
{
	protected $ExpenseAutomationSettingService;

	public function execute($request) 
	{
		$this->expenseDuration = "";
		$this->expenseDuration = ExpenseDao::getAutomation();
		if($_REQUEST['action'] == 'Save')	{
			ExpenseDao::updateAutomation($_REQUEST['duration']);
		}	
		$this->duration = "<select name = duration id = duration> <option value = none> Select </option>";
		for($i = 1; $i <= 12; $i ++)
		{
			if($this->expenseDuration['duration'] == $i)	{
				$this->duration = $this->duration."<option value = {$i} selected> {$i} </option>";
			}
			else	{
				$this->duration = $this->duration."<option value = {$i} > {$i} </option>";
			}
		}
		$this->duration = $this->duration."</select>";
		$this->expenseDuration = ExpenseDao::getAutomation();
		if($_SESSION['isAdmin'] == 'No')
		{
			die('You Dont have Permission to View this page');
		}
	}
}

