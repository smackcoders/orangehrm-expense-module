<?php

class ExpenseAutomationAction extends orangehrmAction 
{
	protected $ExpenseAutomationService;

	public function execute($request) 
	{
		// Getting all the values from ExpenseType table
			
		$curdate = date('Y-m-d');
		$this->expenseType = ExpenseTypeDao::getExpenseType();
		foreach($this->expenseType as $single_expense)
		{	
			
			if($single_expense['automation'] == 1)
			{
				//	Check the created date with the current date
				
				$sub_date = ExpenseTypeDao::diff_date($single_expense['expensetypeId']);
				if($sub_date != 'none')
				{
					ExpenseTypeDao::updateExpenseAutomation($single_expense['expensetypeId']);
				}
			}
		}
		die;
	}
}

