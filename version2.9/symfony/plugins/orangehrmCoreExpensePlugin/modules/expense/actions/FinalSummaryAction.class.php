<?php

class FinalSummaryAction extends orangehrmAction 
{
	    protected $FinalSummaryService;

	    public function execute($request) 
	    {
		// 	Set Session for pagination, if not set

		if( $_SESSION['smackExpensePage'] == '' || !isset($_SESSION['smackExpensePage']) || empty($_SESSION['smackExpensePage']) || $_REQUEST['smack'] == 'First')
		{
			$_SESSION['smackExpensePage'] = 0;
			$_SESSION['smack_finalorder'] = "order by created_time ASC";
		}

		//	For Search using order

		if($_REQUEST['order'] && $_REQUEST['col_name'])
		{
			$_SESSION['smack_finalorder'] = "order by {$_REQUEST['col_name']} {$_REQUEST['order']}";
		}

		$this->values = "";
		$this->filterExpensevalue = "";
		$this->filtervalue = "";
		$_SESSION['expense_search'] = '';
		$this->emp_tosearch   = ExpenseDao::makeEmployeesarray();
		if($_SESSION['isAdmin'] == 'Yes')
		{
			$this->projectName = ExpenseTypeDao::getAllProjects();
			$this->expenseType = ExpenseTypeDao::getExpenseType();
		}
		else
		{	
			$this->expenseType = ExpenseTypeDao::getEmpExpenseType($_SESSION['empNumber']);
			$this->projectName = ExpenseTypeDao::getEmpProjects($_SESSION['empNumber']);
		}

		if($_REQUEST['search'] == true)
		{
			unset($_REQUEST['btnSearch']);
			unset($_REQUEST['search']);
			unset($_REQUEST['smack']);
			$this->Search_Expense = true;
			if($_SESSION['isAdmin'] == 'Yes')
			{
				$this->values = ExpenseDao::searchAdminExpenseList($_REQUEST,$_REQUEST['page']);
			}
			else
			{
				$this->values = ExpenseDao::searchExpenseList($_REQUEST,$_REQUEST['page']);
			}
		}
		else if(isset($_REQUEST['page']))
		{
			if($_SESSION['isAdmin'] == 'Yes')
			{
				$this->values = ExpenseDao::getAdminExpense($_REQUEST['page']);
			}
			else
			{
				$this->values = ExpenseDao::getExpense($_REQUEST['page']);
			}
		}
		else
		{
			if($_SESSION['isAdmin'] == 'Yes')
			{
				$this->values = ExpenseDao::getAdminExpense();
			}
			else
			{
				$this->values = ExpenseDao::getExpense();
			}
		}
	}
}

