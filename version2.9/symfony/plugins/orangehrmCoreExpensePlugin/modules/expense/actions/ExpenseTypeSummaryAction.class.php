<?php

class ExpenseTypeSummaryAction extends orangehrmAction 
{
	    protected $ExpenseTypeSummaryService;

	    public function execute($request) 
	    {
		// 	Set Session for pagination, if not set

		if( $_SESSION['smackExpenseTypeSummary'] == '' || !isset($_SESSION['smackExpenseTypeSummary']) || empty($_SESSION['smackExpenseTypeSummary']) || $_REQUEST['smack'] == 'First')
		{
			$_SESSION['smackExpenseTypeSummary'] = 0;
		}

		$this->values = "";

		if(isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] == 'Yes') 
		{
				$this->values = ExpenseDao::getExpenseTypeSummary($_REQUEST['page']);
		}
		else
		{
			die("You don't have permission to view this page");
		}
	}
}

