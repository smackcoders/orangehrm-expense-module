<link href="<?php echo public_path('../../themes/orange/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../../themes/orange/css/ui-lightness/jquery-ui-1.7.2.custom.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/expense.css')?>" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.core.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.datepicker.js')?>"></script>

<?php echo stylesheet_tag('../orangehrmCoreExpensePlugin/css/applyExpenseSuccess'); ?>
<?php echo stylesheet_tag('orangehrm.datepicker.css') ?>
<?php echo javascript_include_tag('orangehrm.datepicker.js')?>
<script type = "text/javascript">

checked=false;
function checkedAll (frm1) 
{
	var aa= document.getElementById('frmProjectType');
	if (checked == false)
        {
        	checked = true
        }
        else
        {
		checked = false
        }
	for(var i =0; i < aa.elements.length; i++) 
	{
		aa.elements[i].checked = checked;
	}
}

function confirmDelete()
{
	var checked = false;
	var check = document.getElementById('frmProjectType');
	for(var i = 0; i < check.elements.length; i ++) 
	{
		checked = check.elements[i].checked;
		if(checked == true)
		{
			break;
		}
	}
	
	if(checked == false)
	{
		document.getElementById('showMessage').innerHTML = 'Please Select any values to delete';
		return false;
	}

	if(checked == true)
	{
		var r = confirm("Are you sure you want to Delete ?");
		if(r == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	} 
}

function redirect()
{
	window.location.href = "defineExpenseType";
}
</script>
  
<html>
<body>
<?php if($_SESSION['messsage'] != '') { ?>
	<div  id="messagebar" class="messageBalloon_success" style="margin-left: 16px;width: 470px;"> 
		<?php if($_SESSION['messsage'] == 'delete') { ?> <h2>  Expense Type Successfully deleted </h2> <?php  } 
		      else if($_SESSION['messsage'] == 'create') { ?> <h2> Expense Project Created Successfully </h2> <?php  } 
		      else if($_SESSION['messsage'] == 'update') { ?> <h2> Expense Project Updated Successfully </h2> <?php  } $_SESSION['messsage'] = ''; ?> </div>  <?php } ?>
<div style = "padding-left:10px;padding-top:10px;">
<div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
<div class = "formpage">
<div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
<div class = "outerbox" style = "width:505px;">
<form name = "frmProjectType" id = "frmProjectType" action = "" method = "post" id = "frmProjectType" onsubmit = "return confirmDelete();" >
       <input type = "hidden" name = "action" value = "edit">
       <input type = "hidden" name = "module_name" value = "Expense"> 
       <input type = "hidden" name = "employee_id" value = "<?php echo $userId; ?>"> 
       <div class = "mainHeading">
	       <h2> Show Expense Project </h2>
	</div>
<table border="0" cellspacing="0" cellpadding="0" class="data-table" > 
	<div class="actionbar">
		<div class="actionbuttons">
			<input id="btnAdd" class="addbutton" type="button" value="Add" name="btnAdd" onclick = "redirect();">
			<input id="btnDel" class="delbutton" type="submit" value="Delete" name="btnDel">
			<input id="hdnEditId" type="hidden" value="" name="hdnEditId">
		</div>
	</div>
	<tr class="innercheckbox" style = "background-color:#FAD163"> 
		<td class = "paddingp"> <input type = "checkbox" name = "selectAll" id = "selectAll"  onclick = "checkedAll('frmProjectType')"> </td> 
		<td> Expense Name </td> 
	</tr>

<?php 
foreach($values as $val) 
{
		if($i % 2 == 0)
		{ 
?>			<tr valign="top"> 
				<td style = "width:10%;"> <input type = "checkbox" name = "selectExpense[]" id = "selectExpense" value = "<?php echo $val['expensetypeId'];?>">  </td> <td> <a href = "./editExpenseType/<?php echo $val['expensetypeId']; ?>" > <?php echo $val['expenseName']; ?> </td>
			</tr>
<?php 		} 
		else
		{
?>			<tr  style = "background-color:#EEEEEE" valign="top"> 
				<td style = "width:10%;"> <input type = "checkbox"  name = "selectExpense[]" id = "selectExpense" value = "<?php echo $val['expensetypeId'];?>" >  </td> <td> <a href = "./editExpenseType/<?php echo $val['expensetypeId']; ?>" > <?php echo $val['expenseName']; ?> </td>
			</tr>
<?php		} 
		$i ++;
}
?>
	 </tr>
</table>
</div>
</div>
</div>
</form>
</body>
</html>
