<link href="<?php echo public_path('../../themes/orange/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../../themes/orange/css/ui-lightness/jquery-ui-1.7.2.custom.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/expense.css')?>" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.core.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.datepicker.js')?>"></script>

<script type = "text/javascript">

function checkapply()
{
	var type = document.getElementById('expense').value;
	var amount = document.getElementById('amount').value;
	var date = document.getElementById('expensedate').value;
	var checkamt = isNaN(amount);
	if(type == 0 || amount.length == 0 || checkamt == true || date == 'yy-mm-dd' || date.length == 0)
	{
		document.getElementById('showMessage').innerHTML = "Please Fill mandotary fields";
		return false;
	}
}
</script>

<script type = "text/javascript">

var datepickerDateFormat = 'yy-mm-dd';
$(document).ready(function(){
var rDate = trim($("#expensedate").val());
if (rDate == '') {
$("#expensedate").val(datepickerDateFormat);
}

//Bind date picker
daymarker.bindElement("#expensedate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#expensedate_Button').click(function(){
daymarker.show("#expensedate");
});
});

</script>

<?php echo stylesheet_tag('../orangehrmCoreExpensePlugin/css/applyExpenseSuccess'); ?>
<?php echo stylesheet_tag('orangehrm.datepicker.css'); ?>
<?php echo javascript_include_tag('orangehrm.datepicker.js'); ?>
  
<html>
<body>
<?php if($_SESSION['messsage'] != '') { ?>
	<div  id="messagebar" class="messageBalloon_success" style="margin-left: 16px;width: 470px;"> 
		<?php if($_SESSION['messsage'] == 'create') { ?> <h2> Applied Expense Successfully </h2> <?php  } 
		      $_SESSION['messsage'] = ''; ?> </div>  <?php } ?>
<div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
<div class = "formpage">
<div class = "outerbox">
<form name = "frmExpense" onsubmit = "return checkapply();" action = "" method = "post" id = "frmExpense" >
       <input type = "hidden" name = "action" value = "create"> 
       <input type = "hidden" name = "empId" value = "<?php echo ExpenseDao::getEmployeeIdById($_SESSION['user']); ?>" >
       <div class = "mainHeading">
		<h2> Apply Expense </h2>
       </div>
       <table border="0" cellspacing="0" cellpadding="0" style="margin-left: 18px;"> 
 		<tr valign="top"> 
			<td> Expense Name <span style = "color:red"> * </span> </td> 
			<td> <select name = "expense" id = "expense"> <option value = '0'> Select </option> <?php echo htmlspecialchars_decode($expenseTypes); ?> </select></td>
		</tr>
		<tr valign="top">
			<td> Date <span style = "color:red"> * </span> </td> 
			<td> <input type = "textbox" readonly name = "expensedate" id = "expensedate"> <input id="expensedate_Button" class="calendarBtn" type="button" value="" style="float: none; display: inline; margin-left: 6px;"> </td> 					
		</tr>
		<tr valign="top">
		 	<td> Amount <span style = "color:red"> * </span> </td> 
			<td> <input type = "text" name = "amount" id = "amount"> </td>
		</tr>
		<tr valign="top">
		 	<td> Comments </td> 
			<td> <textarea name = "comments" id = "comments"> </textarea> </td>
		</tr>
	</table>
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Save"  class="applybutton" id="saveBtn" > </div>
</div>
</div>
</div>
</div>
</form>
</body>
</html>

