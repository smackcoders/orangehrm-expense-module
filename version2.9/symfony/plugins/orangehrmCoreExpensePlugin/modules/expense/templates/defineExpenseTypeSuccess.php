<link href="<?php echo public_path('../../themes/orange/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../../themes/orange/css/ui-lightness/jquery-ui-1.7.2.custom.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/autoComplete.css')?>" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.core.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.datepicker.js')?>"></script>

<?php echo stylesheet_tag('../orangehrmCoreExpensePlugin/css/applyExpenseSuccess'); ?>
<?php echo stylesheet_tag('orangehrm.datepicker.css') ?>
<?php echo javascript_include_tag('orangehrm.datepicker.js')?>

<script type = "text/javascript">
function checkExpenseNam()
{
	var exname = document.getElementById('name').value;
	var project = document.getElementById('project').value;
	var x = document.getElementById("project").selectedIndex;
	var projectname = document.getElementById('project').options;
	var data;
	if(exname.length != 0 && project != 'none')
	{
		data = "&name="+exname+"&projectid="+project;
		jQuery.ajax({
			url: 'checkExpenseName',
			type: 'post',
			data: data,
			success: function(response)
			{
				if(response == 2) 
				{
					document.getElementById('showMessage').innerHTML = exname+" Already Exist for the Project "+projectname[x].text;
					document.getElementById('name').style.borderColor = 'red';
					document.getElementById('name').value = '';
					return false;
				}
				else
				{
					document.getElementById('showMessage').innerHTML = "";
					document.getElementById('name').style.borderColor = 'black';
					return true;
				}
			}
		});
	}
}
</script>
  
<html>
<body>
<div style = "padding-left:10px;padding-top:10px;">
<div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
<div class = "formpage">
<div class = "outerbox" style="width: 505px">
<form name = "frmProjectType" action = "" method = "post" id = "frmProjectType" onsubmit = "return checkCreateProject();" >
       <input type = "hidden" name = "action" value = "create">
       <div class = "mainHeading">
		<h2> Create Expense Project </h2>
	</div>
       <table border = "0" cellspacing = "0" cellpadding = "0" style = "margin-left: 18px;" class = "outerMost" > 
 
		<tr valign="top"> 
			<td> Expense Name <span style = "color:red;"> * </span> </td> <td> <input type = "text" name = "name" id = "name" onblur = "checkExpenseNam();"></td>
		</tr>
		<tr valign="top">
			<td> Project Name <span style = "color:red;"> * </span> </td> <td> <select id = "project" name = "project" onchange = "checkExpenseNam();"> <option value = "none" selected> Select </option>  <?php echo htmlspecialchars_decode($Project); ?> </select> </td>
		</tr>
		<tr valign="top">
			<td> Description </td> 	<td> <textarea name = "desc" id = "desc"> </textarea> </td>
		</tr>
		<tr valign="top">
		 	<td> Budget <span style = "color:red;">*</span> </td> <td> <input type = "text" name = "budget" id = "budget"> </td> <td> <input type = "checkbox" name = "automation" id = "automation" checked = "checked" > </td> <td>  Update Monthly </td>
		</tr>
	</table>
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Save"  class="applybutton" id="saveBtn" > </div>
</div>
</div>
</div>
</form>
</body>
</html>
<script type = "text/javascript">

function checkCreateProject()
{
	var exname = document.getElementById('name').value;
	var budget = document.getElementById('budget').value;
	var x = document.getElementById("project").selectedIndex;
	var project = document.getElementById('project').value;
	var projectname = document.getElementById('project').options;
	var checkBud = isNaN(budget.trim());	
	if(exname.trim().length == 0 || budget.trim().length == 0 || project == 'none') 
	{
		document.getElementById('showMessage').innerHTML = "Please fill Mandatory Fields";
		return false;
	}
	else if(checkBud == true)
	{
		document.getElementById('showMessage').innerHTML = "Budget should be in Number";
		return false;
	}
	else if(budget.trim().length > 10)
	{
		document.getElementById('showMessage').innerHTML = "Number should be Less than 10";
		return false;
	}
	else
	{
		var data;
		if(exname.length != 0 && project != 'none')
		{
			data = "&name="+exname+"&projectid="+project;
			jQuery.ajax({
				url: 'checkExpenseName',
				type: 'post',
				data: data,
				success: function(response)
				{
					if(response == 2) 
					{
						document.getElementById('showMessage').innerHTML = exname+" Already Exist for the project "+projectname[x].text;
						document.getElementById('name').style.borderColor = 'red';
						document.getElementById('name').value = '';
						return false;
					}
					else
					{
						document.getElementById('showMessage').innerHTML = "";
						document.getElementById('name').style.borderColor = 'black';
						return true;
					}
				}
			});
		}
	}
}
</script>
