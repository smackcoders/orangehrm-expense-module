<link href="<?php echo public_path('../../themes/orange/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../../themes/orange/css/ui-lightness/jquery-ui-1.7.2.custom.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/autoComplete.css')?>" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.core.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.datepicker.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/jquery.autocomplete.js') ?>"></script>
<html>
<body>
	<div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
	<div class = "outerbox" style="width:600px;">
		<form name = "searchExpenseTypeSummary" action = "#" method = "post" id = "frmExpense">
			<div class="mainHeading">
				<h2> Expense Type Summary </h2>
			</div>
		</form>
		</div>
	<div>
</div>
<form name = "frmExpense" action = "#" method = "post" id = "frmExpense" style = "width:auto;" >
<div class="outerbox" style="width:600px;">
       <input type = "hidden" name = "action" value = "save">
       <input type = "hidden" name = "empId" value = "<?php echo ExpenseDao::getEmployeeIdById($_SESSION['user']); ?>" >
       <table  class="data-table" style="border-collapse: collapse; width: 100%; text-align: left;"> 
		<tr style = "background-color:#FAD163;color:#444444"> 	
			<th class = "tab_th"> Expense Name </th> 
			<th class = "tab_th"> Expense Amount </th> 
		</tr>
<?php $chkeven = 1; ?>
<?php foreach($values as $single) 
      { ?>
		<tr <?php if($chkeven % 2 == 0) { ?> class="even trHover" <?php } if($chkeven % 2 == 1) { ?> class="odd trHover" <?php } ?> >
			<?php $chkeven ++; ?>
			<td class = "tab_td"> <a href = "./editExpenseType/<?php echo $single['expense_type_id']; ?>" > <?php echo ExpenseDao::getExpenseTypeById($single['expense_type_id']); ?> </a> </td>
			<td class = "tab_td"> <?php echo $single['amount']; ?> </td>
		</tr>
<?php } ?>
	</table>
	<div class="formbuttons paddingLeft" > 
		<span style = "margin-left:35%;">  
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($_SESSION['smackExpenseTypeSummary'] == 0) { ?> disabled = "disabled" <?php } ?> value="Previous" name="btnReset">
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($next_disable) { ?> disabled = "disabled" <?php } ?>  value="Next" name="btnReset"> 
		</span>
	</div>

		<script type = "text/javascript">
			function page(val)
			{
				if(val == 'Next')
				{
					window.location.href = 'ExpenseTypeSummary?page=Next';
				}
				else if(val == 'Previous')
				{
					window.location.href = 'ExpenseTypeSummary?page=Previous';
				}
			}
		</script>
	</div>
	<div> 
</div>
</div>
</div>
</form>
</div>
</body>
</html>

