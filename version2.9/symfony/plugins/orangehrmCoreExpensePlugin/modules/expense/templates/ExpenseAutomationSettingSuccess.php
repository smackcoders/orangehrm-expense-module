<link href="<?php echo public_path('../../themes/orange/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../../themes/orange/css/ui-lightness/jquery-ui-1.7.2.custom.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/autoComplete.css')?>" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.core.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.datepicker.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/jquery.autocomplete.js') ?>"></script>
<?php echo stylesheet_tag('../orangehrmCoreExpensePlugin/css/applyExpenseSuccess'); ?>
<?php echo stylesheet_tag('orangehrm.datepicker.css'); ?>
<?php echo javascript_include_tag('orangehrm.datepicker.js'); ?>

<html>
<body>
<div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
<div class = "formpage">
<div class="outerbox" style="padding-right: 15px;">
<?php  if($_REQUEST['action'] == 'Edit') 	{	?>
	<form name = "ExpenseAutomation" action = "#" method = "post" id = "ExpenseAutomation" style = "width:auto;" onsubmit = "return checkAutomation();">
       <input type = "hidden" name = "action" value = "Save">
       <div class = "mainHeading">
		<h2> Edit Expense Automation </h2>
       </div>
       <table style="border-collapse: collapse; width: 100%; text-align: left;"> 
       		<tr> 
			<td> Month Duration </td>
			<td> <?php echo htmlspecialchars_decode($duration); ?> </td>
		</tr>
       </table>
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Save"  class="applybutton" id="saveBtn" > </div> </td>
	</form>

<?php	}
	else 	{	?>
       <form name = "ExpenseAutomation" action = "#" method = "post" id = "ExpenseAutomation" style = "width:auto;" >
       <input type = "hidden" name = "action" value = "Edit">
       <div class = "mainHeading">
		<h2> Show Expense Automation </h2>
       </div>
       <table style="border-collapse: collapse; width: 100%; text-align: left;"> 
       		<tr> 
			<td> Month Duration </td>
			<td> <?php if(isset($expenseDuration)) { echo $expenseDuration['duration']; } ?> </td>
		</tr>
       </table>
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Edit"  class="applybutton" id="saveBtn" > </div> </td>
	</form>

<?php 	}    ?>

</div>
</div>
</form>
</body>
</html>
	<script type = "text/javascript">
		function checkAutomation()
		{
			var duration = document.getElementById('duration').value;
			if(duration == 'none')	{
				document.getElementById('showMessage').innerHTML = "Please select Month Duration";
				return false;
			}
		}
	</script>
