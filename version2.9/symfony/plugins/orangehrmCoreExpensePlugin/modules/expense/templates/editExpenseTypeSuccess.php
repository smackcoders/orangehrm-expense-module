<link href="<?php echo public_path('../../themes/orange/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../../themes/orange/css/ui-lightness/jquery-ui-1.7.2.custom.css')?>" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.core.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.datepicker.js')?>"></script>
<style type="text/css">
.error_list {
    color: #ff0000;
}

td
{
	padding-left:5px;
	padding-top:5px;
}

.maincontent
{
    -moz-border-bottom-colors: none;
    -moz-border-image: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: white;
    border-color: #FAD163;
    border-style: solid;
    border-width: 0 2px;
    height: auto;
	width:500px;
}

.outerMost {
    margin-left: 15px;
    margin-top: 15px;
    width: 400px;
}
</style>

<?php echo stylesheet_tag('../orangehrmCoreExpensePlugin/css/applyExpenseSuccess'); ?>
<?php echo stylesheet_tag('orangehrm.datepicker.css') ?>
<?php echo javascript_include_tag('orangehrm.datepicker.js')?>
  
<html>
<body>
<div style = "padding-left:10px;padding-top:10px;"><div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
<form name = "frmProjectType" action = "" method = "post" id = "frmProjectType" onsubmit = "return checkCreateProject();" >
       <input type = "hidden" name = "action" value = "update">
       <input type = "hidden" name = "module_name" value = "Expense">
<div class = "formpage">
<div class = "outerbox" style="width:505px;"> 
       <input type = "hidden" name = "employee_id" value = "<?php echo $userId; ?>"> 
       <input type = "hidden" name = "expenseid" value = "<?php echo $values['expensetypeId']; ?>"> 
       <div class = "mainHeading">
	       <h2> <?php if($_REQUEST['edit'] == 1) { ?> Edit Expense Project <?php } else { echo "Show Expense Project"; } ?> </h2>
	</div>
       <table border="0" cellspacing="0" cellpadding="0" style="margin-left: 18px;" class="outerMost" > 
 
		<tr valign="top"> 
			<td> Expense Name <?php if($_REQUEST['edit'] == 1) { ?> <span style = "color:red;">*</span> <?php } ?> </td> <td> <?php if($_REQUEST['edit'] == 1) { ?> <input type = "text" name = "name" id = "name" value = "<?php echo $values['expenseName'] ?>"> <?php } else { echo $values['expenseName']; } ?></td>
		</tr>
		<tr valign="top">
			<td> Project Name <?php if($_REQUEST['edit'] == 1) { ?> <span style = "color:red;">*</span> <?php } ?> </td> <td> <?php if($_REQUEST['edit'] == 1) { ?> <select id = "project" name = "project"> <option value = "none" > Select </option>  <?php echo htmlspecialchars_decode($Project); ?> </select> <?php } else { echo ExpenseDao::getProjectNameById($values['projectId']); } ?> </td>
		</tr>
		<tr valign="top">
			<td> Description </td> 	<td> <?php if($_REQUEST['edit'] == 1) { ?> <textarea name = "desc" id = "desc">  <?php echo $values['description'] ?> </textarea>  <?php } else { echo $values['description']; } ?> </td>
		</tr>
		<tr valign="top">
		 	<td> Budget <?php if($_REQUEST['edit'] == 1) { ?> <span style = "color:red;">*</span> <?php } ?> <td> <?php if($_REQUEST['edit'] == 1) { ?> <input type = "text" name = "budget" id = "budget" <?php if($values['budget']) { ?> value = "<?php echo $values['budget'] ?>"  <?php } ?> >  <?php } else { echo $values['budget']; } ?> </td> 
			<?php if($values['automation'] == 1) { $auto = 'Yes';  } else { $auto = 'No'; } ?>
			<td> <?php if($_REQUEST['edit'] == 1) { ?> <input type = "checkbox" name = "automation" id = "automation" <?php if($values['automation'] == 1) { ?> checked = "checked" >  <?php } ?></td> <td> Update Monthly <?php } else { echo "Update monthly ->".$auto;  } ?> </td> 
		</tr>
	</table>
	<?php if($_REQUEST['edit'] == 1) { ?> <div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Update"  class="applybutton" id="saveBtn" > </div> <?php } else { ?> <div class="formbuttons paddingLeft" >  <input type = "button"  value = "Edit"  class="applybutton" id="saveBtn" onclick = "redirect_cur();" > </div> <?php } ?>
</div>
</div>
</dvi>
</form>
</body>
</html>
<script type = "text/javascript">
function redirect_cur()
{
	window.location.href = 'editExpenseType?edit=1/<?php echo $id; ?>';
}

function checkCreateProject()
{
	var exname = document.getElementById('name').value;
	var budget = document.getElementById('budget').value;
	var managers = document.getElementById('managers').value;
	var users = document.getElementById('users').value;
	var checkBud = isNaN(budget.trim());	
	if(exname.trim().length == 0 || budget.trim().length == 0 || users.trim().length == 0 || managers.trim().length == 0) 
	{
		document.getElementById('showMessage').innerHTML = "Please fill Mandatory Fields";
		return false;
	}
	else if(checkBud == true)
	{
		document.getElementById('showMessage').innerHTML = "Budget should be in Number";
		return false;
	}
	else if(budget.trim().length > 10)
	{
		document.getElementById('showMessage').innerHTML = "Number should be Less than 10";
		return false;
	}
	return true;
}
</script>
