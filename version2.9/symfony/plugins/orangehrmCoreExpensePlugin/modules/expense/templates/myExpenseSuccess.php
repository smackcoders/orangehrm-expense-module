<link href="<?php echo public_path('../../themes/orange/css/expense.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../../themes/orange/css/ui-lightness/jquery-ui-1.7.2.custom.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path('../plugins/orangehrmCoreExpensePlugin/web/css/expense.css')?>" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.core.js')?>"></script>
<script type="text/javascript" src="<?php echo public_path('../../scripts/jquery/ui/ui.datepicker.js')?>"></script>

<?php echo stylesheet_tag('../orangehrmCoreExpensePlugin/css/applyExpenseSuccess'); ?>
<?php echo stylesheet_tag('orangehrm.datepicker.css'); ?>
<?php echo javascript_include_tag('orangehrm.datepicker.js'); ?>

<script type = "text/javascript">
function clearSearch()
{
	document.getElementById('fromdate').value = "";
	document.getElementById('todate').value = "";
	document.getElementById('pending').checked = false;
	document.getElementById('approved').checked = false;
	document.getElementById('cancelled').checked = false;
	document.getElementById('rejected').checked = false;
}

function checkSearch()
{
	var from = document.getElementById('fromdate').value;
	var to = document.getElementById('todate').value;
	var extype = document.getElementById('extype').value;
	var pending = document.getElementById('pending').checked;
	var approved = document.getElementById('approved').checked;
	var cancelled = document.getElementById('cancelled').checked;
	var rejected = document.getElementById('rejected').checked;
	if(from == 'yy-mm-dd' && to == 'yy-mm-dd' && pending == false && approved == false && cancelled == false && rejected == false && extype == 'none')
	{
		document.getElementById('showMessage').innerHTML = "Please Select any values to search";
		return false;
	}
	else if(from != 'yy-mm-dd' && to != 'yy-mm-dd')
	{
		if(from > to)
		{
			document.getElementById('showMessage').innerHTML = "From date should be greater than To date";
			return false;			
		}
	}
	else
	{
		return true;
	}
}
</script>

<script type = "text/javascript">

var datepickerDateFormat = 'yy-mm-dd';
$(document).ready(function(){
var rDate = trim($("#todate").val());
if (rDate == '') {
$("#todate").val(datepickerDateFormat);
}
//Bind date picker
daymarker.bindElement("#todate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#todate_Button').click(function(){
daymarker.show("#todate");
});
});

$(document).ready(function(){
var rDate = trim($("#fromdate").val());
if (rDate == '') {
$("#fromdate").val(datepickerDateFormat);
}
//Bind date picker
daymarker.bindElement("#fromdate",
{
onSelect: function(date){
},
dateFormat : datepickerDateFormat,
onClose: function(){
$(this).valid();
}
});
$('#fromdate_Button').click(function(){
daymarker.show("#fromdate");
});
});

</script>  
<?php
	// Making redirection for search

	$redirect_next = ExpenseDao::searchnextRedirect();
	$redirect_prev = ExpenseDao::searchprevRedirect();
	if($expenseType)
	{
		$expenseTypes = '<select name = "extype" id = "extype" > <option value = "none"> Select </option>';
		foreach($expenseType as $singleExpenseType)
		{ 
			if($singleExpenseType['expensetypeId'] == $_SESSION['expense_search']['expense_type_id'])	
			{
				$expenseTypes = $expenseTypes."<option selected value = \"".$singleExpenseType['expensetypeId']."\">".$singleExpenseType['expenseName']."</option>";
			}
			else
			{
				$expenseTypes = $expenseTypes."<option value = \"".$singleExpenseType['expensetypeId']."\">".$singleExpenseType['expenseName']."</option>";
			}
		}
		$expenseTypes = $expenseTypes."</select>";
	}

?>

<html>
<body>
<?php if($_SESSION['messsage'] != '') { ?>
	<div  id="messagebar" class="messageBalloon_success" style="margin-left: 16px;width: 470px;"> 
		<?php if($_SESSION['messsage'] == 'create') { ?> <h2> Applied Expense Successfully </h2> <?php  }
			else if($_SESSION['messsage'] == 'update') { ?> <h2> Expense Updated Successfully </h2> <?php  } 
		      $_SESSION['messsage'] = ''; ?> </div>  <?php } ?>
<div style = "margin-left:40%; color:red;" id = "showMessage"> </div>
<div class = "outerbox">
	<div class="formWrapper">
		<form name = "searchExpenseList" onsubmit = "return checkSearch();" action = "#" method = "post" id = "frmExpense">
			<div class="mainHeading">
				<h2> Expense List </h2>
			</div>
			<table> 
				<input type = "hidden" name = "search" value = "true">
				<tr>
					<td width = "30%"> <label> Expense Name </label> <?php echo htmlspecialchars_decode($expenseTypes); ?>
				</tr>
				<tr>
					<td width = "30%"> <label> From Date </label> <input type = "textbox" readonly name = "fromdate" <?php if($_SESSION['expense_search']['fromdate']) { ?> value="<?php echo $_SESSION['expense_search']['fromdate'] ?>" <?php  } ?> id = "fromdate"> <input id="fromdate_Button" class="calendarBtn" type="button" value=""  style="float: none; display: inline; margin-left: 6px;"> </td> 
					<td width = "30%"> </td>
				</tr>
				<tr>
					<td width = "30%"> <label> To Date </label> <input type = "textbox" readonly name = "todate" id = "todate" <?php if($_SESSION['expense_search']['todate']) { ?> value="<?php echo $_SESSION['expense_search']['todate'] ?>" <?php  } ?> > <input id="todate_Button" class="calendarBtn" type="button" value="" style="float: none; display: inline; margin-left: 6px;"> </td> 
				</tr>
				<tr>
				        <td style = "padding-left:20px;"> Expense With status  <span style = "padding-left:20px;">  Pending </span> <span> <input type = "checkbox" name = "pending" id = "pending" <?php if($_SESSION['expense_search']['pending']) { ?> checked <?php  } ?> >  <span style = "padding-left:20px;"> Approved </span>  <span style = "padding-left:20px;"> <input type = "checkbox" name = "approved" id = "approved" <?php if($_SESSION['expense_search']['approved']) { ?> checked <?php  } ?>> </span>  <span style = "padding-left:20px;"> Cancelled </span>  <span style = "padding-left:20px;"> <input type = "checkbox" name = "cancelled" <?php if($_SESSION['expense_search']['cancelled']) { ?> checked <?php  } ?> id = "cancelled"> </span> <span style = "padding-left:20px;"> Rejected </span> <span style = "padding-left:20px;"> <input type = "checkbox" name = "rejected" id = "rejected" <?php if($_SESSION['expense_search']['rejected']) { ?> checked <?php  } ?>> </span> </td>
				</tr>
			</table>
			<div class="buttonWrapper">
				<input id="btnSearch" class="searchbutton" type = "submit" value="Search" name="btnSearch" >
				<input id="btnReset" class="clearbutton" type="button" onclick = "clearSearch();" value="Reset" name="btnReset">
				<input id="pageNo" type="hidden" value="" name="pageNo">
			</div>
		</form>
	</div>
</div>

<div class = "outerbox">
<form name = "frmExpense" action = "" method = "post" id = "frmExpense" style = "width:auto;" >
       <input type = "hidden" name = "action" value = "save">
       <input type = "hidden" name = "empId" value = "<?php echo ExpenseDao::getEmployeeIdById($_SESSION['user']); ?>" >
       <table  class="data-table" style="border-collapse: collapse; width: 100%; text-align: left;"> 
		<tr style = "background-color:#FAD163;color:#444444"> 	
			<th rowspan="1" style="text-align: left" class = "tab_th"> Expense Name </th> <th rowspan="1" style="text-align: left"> Expense Date </th> <th rowspan="1" style="text-align: left"> Expense Amount </th> <th rowspan="1" style="text-align: left"> Status </th> <th rowspan="1" style="text-align: left"> Comment </th> <th rowspan="1" style="text-align: left"> Action </th>
		</tr>
<?php $chkeven = 1; ?>
<?php foreach($myExpense as $single) 
      {
		$status = ''; 
		foreach($status_expense as $key => $assign_status)
		{
			if($key == $single['expense_status'])
			{
				$status = $status."<option selected value = '{$key}'> {$assign_status} </option>";
			}
			else
			{
				$status = $status."<option value = '{$key}'> {$assign_status} </option>";
			}
		}
		$status = "<select name = 'status' > <option value = '0'> Select Action </option>".$status."</select>";
		$action = '';
		foreach($actions as $key => $split_action)
		{
			$action = $action."<option value = '{$key}'> {$split_action} </option>";
		}
			$action = "<select name = '{$single['expense_id']}-status' >".$action."</select>";
?>
		<tr <?php if($chkeven % 2 == 0) { ?> class="even" <?php } if($chkeven % 2 == 1) { ?> class="odd" <?php } ?> > 	<?php $chkeven++; ?>
			<td class = "tab_td"> <?php echo ExpenseDao::getExpenseNameById($single['expense_type_id']); ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_date']; ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_amount']; ?> </td>
			<td class = "tab_td"> <?php if($single['expense_status'] == 1) 
				   { 
					echo "Pending"; 
				   }
				   else if($single['expense_status'] == -1) 
				   {
					echo "Rejected"; 
				   } 
				   else if($single['expense_status'] == 0) 
				   { 
					echo "Cancelled"; 
				   } 
				   else if($single['expense_status'] == 2) 
				   {
					echo "Accepted"; 
				   } ?> </td>
			<td class = "tab_td"> <?php echo $single['expense_comments']; ?> </td>
			<td class = "tab_td"> <?php echo $action; ?> </td>
		</tr>
<?php } ?>
 
	</table>
	<div class="formbuttons paddingLeft" >  <input type = "submit"  value = "Save"  class="applybutton" id="saveBtn" > 
		<span style = "margin-left:40%;">  
			<?php if($Search_Expense) {   ?>
				<input id="pageno" class="clearbutton" type="button" onclick = "page_search(this.value);" <?php if($_SESSION['smackExpensePage'] == 0) { ?> disabled = "disabled" <?php } ?> value="Previous" name="btnReset">
				<input id="pageno" class="clearbutton" type="button" onclick = "page_search(this.value);" <?php if($next_disable) { ?> disabled = "disabled" <?php } ?>  value="Next" name="btnReset">  
			<?php	} else {  ?>
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($_SESSION['smackExpensePage'] == 0) { ?> disabled = "disabled" <?php } ?> value="Previous" name="btnReset">
			<input id="pageno" class="clearbutton" type="button" onclick = "page(this.value);" <?php if($next_disable) { ?> disabled = "disabled" <?php } ?>  value="Next" name="btnReset">  <?php } ?>
		</span>
	</div>
</form>
</body>
</html>
		<script type = "text/javascript">
			function page(val)
			{
				if(val == 'Next')
				{
					window.location.href = 'myExpense?page=Next';
				}
				else if(val == 'Previous')
				{
					window.location.href = 'myExpense?page=Previous';
				}
			}

			function page_search(val)
			{
				if(val == 'Next')
				{
					window.location.href = "<?php echo $redirect_next; ?>";
				}
				else if(val == 'Previous')
				{
					window.location.href = "<?php echo $redirect_prev; ?>";
				}
			}
		</script>

